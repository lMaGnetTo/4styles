package com.mdev.styles.web.rest;

import com.mdev.styles.Application;
import com.mdev.styles.domain.Wardrobe;
import com.mdev.styles.repository.WardrobeRepository;
import com.mdev.styles.service.ClothService;
import com.mdev.styles.service.StyleService;
import com.mdev.styles.service.WardrobeService;
import com.mdev.styles.service.dto.WardrobeDTO;
import com.mdev.styles.service.mapper.ClothMapper;
import com.mdev.styles.service.mapper.WardrobeMapper;
import com.mdev.styles.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mdev.styles.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WardrobeResource} REST controller.
 */
@SpringBootTest(classes = Application.class)
public class WardrobeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOGO_URL = "AAAAAAAAAA";
    private static final String UPDATED_LOGO_URL = "BBBBBBBBBB";

    @Autowired
    private WardrobeRepository wardrobeRepository;

    @Autowired
    private WardrobeMapper wardrobeMapper;

    @Autowired
    private WardrobeService wardrobeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private ClothService clothService;

    @Autowired
    private StyleService styleService;

    private MockMvc restWardrobeMockMvc;

    private Wardrobe wardrobe;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WardrobeResource wardrobeResource = new WardrobeResource(wardrobeService, clothService, styleService);
        this.restWardrobeMockMvc = MockMvcBuilders.standaloneSetup(wardrobeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Wardrobe createEntity(EntityManager em) {
        Wardrobe wardrobe = new Wardrobe()
            .name(DEFAULT_NAME)
            .logoUrl(DEFAULT_LOGO_URL);
        return wardrobe;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Wardrobe createUpdatedEntity(EntityManager em) {
        Wardrobe wardrobe = new Wardrobe()
            .name(UPDATED_NAME)
            .logoUrl(UPDATED_LOGO_URL);
        return wardrobe;
    }

    @BeforeEach
    public void initTest() {
        wardrobe = createEntity(em);
    }

    @Test
    @Transactional
    public void createWardrobe() throws Exception {
        int databaseSizeBeforeCreate = wardrobeRepository.findAll().size();

        // Create the Wardrobe
        WardrobeDTO wardrobeDTO = wardrobeMapper.toDto(wardrobe);
        restWardrobeMockMvc.perform(post("/api/wardrobes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wardrobeDTO)))
            .andExpect(status().isCreated());

        // Validate the Wardrobe in the database
        List<Wardrobe> wardrobeList = wardrobeRepository.findAll();
        assertThat(wardrobeList).hasSize(databaseSizeBeforeCreate + 1);
        Wardrobe testWardrobe = wardrobeList.get(wardrobeList.size() - 1);
        assertThat(testWardrobe.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testWardrobe.getLogoUrl()).isEqualTo(DEFAULT_LOGO_URL);
    }

    @Test
    @Transactional
    public void createWardrobeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = wardrobeRepository.findAll().size();

        // Create the Wardrobe with an existing ID
        wardrobe.setId(1L);
        WardrobeDTO wardrobeDTO = wardrobeMapper.toDto(wardrobe);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWardrobeMockMvc.perform(post("/api/wardrobes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wardrobeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wardrobe in the database
        List<Wardrobe> wardrobeList = wardrobeRepository.findAll();
        assertThat(wardrobeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWardrobes() throws Exception {
        // Initialize the database
        wardrobeRepository.saveAndFlush(wardrobe);

        // Get all the wardrobeList
        restWardrobeMockMvc.perform(get("/api/wardrobes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wardrobe.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].logoUrl").value(hasItem(DEFAULT_LOGO_URL)));
    }

    @Test
    @Transactional
    public void getWardrobe() throws Exception {
        // Initialize the database
        wardrobeRepository.saveAndFlush(wardrobe);

        // Get the wardrobe
        restWardrobeMockMvc.perform(get("/api/wardrobes/{id}", wardrobe.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(wardrobe.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.logoUrl").value(DEFAULT_LOGO_URL));
    }

    @Test
    @Transactional
    public void getNonExistingWardrobe() throws Exception {
        // Get the wardrobe
        restWardrobeMockMvc.perform(get("/api/wardrobes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWardrobe() throws Exception {
        // Initialize the database
        wardrobeRepository.saveAndFlush(wardrobe);

        int databaseSizeBeforeUpdate = wardrobeRepository.findAll().size();

        // Update the wardrobe
        Wardrobe updatedWardrobe = wardrobeRepository.findById(wardrobe.getId()).get();
        // Disconnect from session so that the updates on updatedWardrobe are not directly saved in db
        em.detach(updatedWardrobe);
        updatedWardrobe
            .name(UPDATED_NAME)
            .logoUrl(UPDATED_LOGO_URL);
        WardrobeDTO wardrobeDTO = wardrobeMapper.toDto(updatedWardrobe);

        restWardrobeMockMvc.perform(put("/api/wardrobes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wardrobeDTO)))
            .andExpect(status().isOk());

        // Validate the Wardrobe in the database
        List<Wardrobe> wardrobeList = wardrobeRepository.findAll();
        assertThat(wardrobeList).hasSize(databaseSizeBeforeUpdate);
        Wardrobe testWardrobe = wardrobeList.get(wardrobeList.size() - 1);
        assertThat(testWardrobe.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testWardrobe.getLogoUrl()).isEqualTo(UPDATED_LOGO_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingWardrobe() throws Exception {
        int databaseSizeBeforeUpdate = wardrobeRepository.findAll().size();

        // Create the Wardrobe
        WardrobeDTO wardrobeDTO = wardrobeMapper.toDto(wardrobe);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWardrobeMockMvc.perform(put("/api/wardrobes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wardrobeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Wardrobe in the database
        List<Wardrobe> wardrobeList = wardrobeRepository.findAll();
        assertThat(wardrobeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWardrobe() throws Exception {
        // Initialize the database
        wardrobeRepository.saveAndFlush(wardrobe);

        int databaseSizeBeforeDelete = wardrobeRepository.findAll().size();

        // Delete the wardrobe
        restWardrobeMockMvc.perform(delete("/api/wardrobes/{id}", wardrobe.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Wardrobe> wardrobeList = wardrobeRepository.findAll();
        assertThat(wardrobeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
