package com.mdev.styles.web.rest;

import com.mdev.styles.Application;
import com.mdev.styles.domain.Style;
import com.mdev.styles.repository.StyleRepository;
import com.mdev.styles.service.StyleService;
import com.mdev.styles.service.dto.StyleDTO;
import com.mdev.styles.service.mapper.StyleMapper;
import com.mdev.styles.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static com.mdev.styles.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StyleResource} REST controller.
 */
@SpringBootTest(classes = Application.class)
public class StyleResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private StyleRepository styleRepository;

    @Mock
    private StyleRepository styleRepositoryMock;

    @Autowired
    private StyleMapper styleMapper;

    @Mock
    private StyleService styleServiceMock;

    @Autowired
    private StyleService styleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restStyleMockMvc;

    private Style style;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StyleResource styleResource = new StyleResource(styleService);
        this.restStyleMockMvc = MockMvcBuilders.standaloneSetup(styleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Style createEntity(EntityManager em) {
        Style style = new Style()
            .name(DEFAULT_NAME)
            .created(DEFAULT_CREATED);
        return style;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Style createUpdatedEntity(EntityManager em) {
        Style style = new Style()
            .name(UPDATED_NAME)
            .created(UPDATED_CREATED);
        return style;
    }

    @BeforeEach
    public void initTest() {
        style = createEntity(em);
    }

    @Test
    @Transactional
    public void createStyle() throws Exception {
        int databaseSizeBeforeCreate = styleRepository.findAll().size();

        // Create the Style
        StyleDTO styleDTO = styleMapper.toDto(style);
        restStyleMockMvc.perform(post("/api/styles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(styleDTO)))
            .andExpect(status().isCreated());

        // Validate the Style in the database
        List<Style> styleList = styleRepository.findAll();
        assertThat(styleList).hasSize(databaseSizeBeforeCreate + 1);
        Style testStyle = styleList.get(styleList.size() - 1);
        assertThat(testStyle.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStyle.getCreated()).isEqualTo(DEFAULT_CREATED);
    }

    @Test
    @Transactional
    public void createStyleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = styleRepository.findAll().size();

        // Create the Style with an existing ID
        style.setId(1L);
        StyleDTO styleDTO = styleMapper.toDto(style);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStyleMockMvc.perform(post("/api/styles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(styleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Style in the database
        List<Style> styleList = styleRepository.findAll();
        assertThat(styleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllStyles() throws Exception {
        // Initialize the database
        styleRepository.saveAndFlush(style);

        // Get all the styleList
        restStyleMockMvc.perform(get("/api/styles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(style.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllStylesWithEagerRelationshipsIsEnabled() throws Exception {
        StyleResource styleResource = new StyleResource(styleServiceMock);
        when(styleServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restStyleMockMvc = MockMvcBuilders.standaloneSetup(styleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restStyleMockMvc.perform(get("/api/styles?eagerload=true"))
        .andExpect(status().isOk());

        verify(styleServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllStylesWithEagerRelationshipsIsNotEnabled() throws Exception {
        StyleResource styleResource = new StyleResource(styleServiceMock);
            when(styleServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restStyleMockMvc = MockMvcBuilders.standaloneSetup(styleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restStyleMockMvc.perform(get("/api/styles?eagerload=true"))
        .andExpect(status().isOk());

            verify(styleServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getStyle() throws Exception {
        // Initialize the database
        styleRepository.saveAndFlush(style);

        // Get the style
        restStyleMockMvc.perform(get("/api/styles/{id}", style.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(style.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStyle() throws Exception {
        // Get the style
        restStyleMockMvc.perform(get("/api/styles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStyle() throws Exception {
        // Initialize the database
        styleRepository.saveAndFlush(style);

        int databaseSizeBeforeUpdate = styleRepository.findAll().size();

        // Update the style
        Style updatedStyle = styleRepository.findById(style.getId()).get();
        // Disconnect from session so that the updates on updatedStyle are not directly saved in db
        em.detach(updatedStyle);
        updatedStyle
            .name(UPDATED_NAME)
            .created(UPDATED_CREATED);
        StyleDTO styleDTO = styleMapper.toDto(updatedStyle);

        restStyleMockMvc.perform(put("/api/styles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(styleDTO)))
            .andExpect(status().isOk());

        // Validate the Style in the database
        List<Style> styleList = styleRepository.findAll();
        assertThat(styleList).hasSize(databaseSizeBeforeUpdate);
        Style testStyle = styleList.get(styleList.size() - 1);
        assertThat(testStyle.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStyle.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingStyle() throws Exception {
        int databaseSizeBeforeUpdate = styleRepository.findAll().size();

        // Create the Style
        StyleDTO styleDTO = styleMapper.toDto(style);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStyleMockMvc.perform(put("/api/styles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(styleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Style in the database
        List<Style> styleList = styleRepository.findAll();
        assertThat(styleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStyle() throws Exception {
        // Initialize the database
        styleRepository.saveAndFlush(style);

        int databaseSizeBeforeDelete = styleRepository.findAll().size();

        // Delete the style
        restStyleMockMvc.perform(delete("/api/styles/{id}", style.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Style> styleList = styleRepository.findAll();
        assertThat(styleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
