package com.mdev.styles.web.rest;

import com.mdev.styles.Application;
import com.mdev.styles.domain.Cloth;
import com.mdev.styles.repository.ClothRepository;
import com.mdev.styles.service.ClothService;
import com.mdev.styles.service.dto.ClothDTO;
import com.mdev.styles.service.mapper.ClothMapper;
import com.mdev.styles.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mdev.styles.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ClothResource} REST controller.
 */
@SpringBootTest(classes = Application.class)
public class ClothResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FAVOURITE = false;
    private static final Boolean UPDATED_FAVOURITE = true;

    private static final Boolean DEFAULT_AVAILABLE = false;
    private static final Boolean UPDATED_AVAILABLE = true;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    @Autowired
    private ClothRepository clothRepository;

    @Autowired
    private ClothMapper clothMapper;

    @Autowired
    private ClothService clothService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClothMockMvc;

    private Cloth cloth;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClothResource clothResource = new ClothResource(clothService);
        this.restClothMockMvc = MockMvcBuilders.standaloneSetup(clothResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cloth createEntity(EntityManager em) {
        Cloth cloth = new Cloth()
            .name(DEFAULT_NAME)
            .favourite(DEFAULT_FAVOURITE)
            .available(DEFAULT_AVAILABLE)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE);
        return cloth;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cloth createUpdatedEntity(EntityManager em) {
        Cloth cloth = new Cloth()
            .name(UPDATED_NAME)
            .favourite(UPDATED_FAVOURITE)
            .available(UPDATED_AVAILABLE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);
        return cloth;
    }

    @BeforeEach
    public void initTest() {
        cloth = createEntity(em);
    }

    @Test
    @Transactional
    public void createCloth() throws Exception {
        int databaseSizeBeforeCreate = clothRepository.findAll().size();

        // Create the Cloth
        ClothDTO clothDTO = clothMapper.toDto(cloth);
        restClothMockMvc.perform(post("/api/cloths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothDTO)))
            .andExpect(status().isCreated());

        // Validate the Cloth in the database
        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList).hasSize(databaseSizeBeforeCreate + 1);
        Cloth testCloth = clothList.get(clothList.size() - 1);
        assertThat(testCloth.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCloth.isFavourite()).isEqualTo(DEFAULT_FAVOURITE);
        assertThat(testCloth.isAvailable()).isEqualTo(DEFAULT_AVAILABLE);
        assertThat(testCloth.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testCloth.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createClothWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clothRepository.findAll().size();

        // Create the Cloth with an existing ID
        cloth.setId(1L);
        ClothDTO clothDTO = clothMapper.toDto(cloth);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClothMockMvc.perform(post("/api/cloths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cloth in the database
        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCloths() throws Exception {
        // Initialize the database
        clothRepository.saveAndFlush(cloth);

        // Get all the clothList
        restClothMockMvc.perform(get("/api/cloths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cloth.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].favourite").value(hasItem(DEFAULT_FAVOURITE.booleanValue())))
            .andExpect(jsonPath("$.[*].available").value(hasItem(DEFAULT_AVAILABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }
    
    @Test
    @Transactional
    public void getCloth() throws Exception {
        // Initialize the database
        clothRepository.saveAndFlush(cloth);

        // Get the cloth
        restClothMockMvc.perform(get("/api/cloths/{id}", cloth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cloth.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.favourite").value(DEFAULT_FAVOURITE.booleanValue()))
            .andExpect(jsonPath("$.available").value(DEFAULT_AVAILABLE.booleanValue()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)));
    }

    @Test
    @Transactional
    public void getNonExistingCloth() throws Exception {
        // Get the cloth
        restClothMockMvc.perform(get("/api/cloths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCloth() throws Exception {
        // Initialize the database
        clothRepository.saveAndFlush(cloth);

        int databaseSizeBeforeUpdate = clothRepository.findAll().size();

        // Update the cloth
        Cloth updatedCloth = clothRepository.findById(cloth.getId()).get();
        // Disconnect from session so that the updates on updatedCloth are not directly saved in db
        em.detach(updatedCloth);
        updatedCloth
            .name(UPDATED_NAME)
            .favourite(UPDATED_FAVOURITE)
            .available(UPDATED_AVAILABLE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);
        ClothDTO clothDTO = clothMapper.toDto(updatedCloth);

        restClothMockMvc.perform(put("/api/cloths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothDTO)))
            .andExpect(status().isOk());

        // Validate the Cloth in the database
        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList).hasSize(databaseSizeBeforeUpdate);
        Cloth testCloth = clothList.get(clothList.size() - 1);
        assertThat(testCloth.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCloth.isFavourite()).isEqualTo(UPDATED_FAVOURITE);
        assertThat(testCloth.isAvailable()).isEqualTo(UPDATED_AVAILABLE);
        assertThat(testCloth.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testCloth.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingCloth() throws Exception {
        int databaseSizeBeforeUpdate = clothRepository.findAll().size();

        // Create the Cloth
        ClothDTO clothDTO = clothMapper.toDto(cloth);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClothMockMvc.perform(put("/api/cloths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cloth in the database
        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCloth() throws Exception {
        // Initialize the database
        clothRepository.saveAndFlush(cloth);

        int databaseSizeBeforeDelete = clothRepository.findAll().size();

        // Delete the cloth
        restClothMockMvc.perform(delete("/api/cloths/{id}", cloth.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cloth> clothList = clothRepository.findAll();
        assertThat(clothList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
