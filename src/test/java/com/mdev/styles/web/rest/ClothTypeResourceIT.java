package com.mdev.styles.web.rest;

import com.mdev.styles.Application;
import com.mdev.styles.domain.ClothType;
import com.mdev.styles.repository.ClothTypeRepository;
import com.mdev.styles.service.ClothTypeService;
import com.mdev.styles.service.dto.ClothTypeDTO;
import com.mdev.styles.service.mapper.ClothTypeMapper;
import com.mdev.styles.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mdev.styles.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ClothTypeResource} REST controller.
 */
@SpringBootTest(classes = Application.class)
public class ClothTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DEEPFASHION_ALIAS = "AAAAAAAAAA";
    private static final String UPDATED_DEEPFASHION_ALIAS = "BBBBBBBBBB";

    @Autowired
    private ClothTypeRepository clothTypeRepository;

    @Autowired
    private ClothTypeMapper clothTypeMapper;

    @Autowired
    private ClothTypeService clothTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClothTypeMockMvc;

    private ClothType clothType;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClothTypeResource clothTypeResource = new ClothTypeResource(clothTypeService);
        this.restClothTypeMockMvc = MockMvcBuilders.standaloneSetup(clothTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClothType createEntity(EntityManager em) {
        ClothType clothType = new ClothType()
            .name(DEFAULT_NAME)
            .deepfashionAlias(DEFAULT_DEEPFASHION_ALIAS);
        return clothType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClothType createUpdatedEntity(EntityManager em) {
        ClothType clothType = new ClothType()
            .name(UPDATED_NAME)
            .deepfashionAlias(UPDATED_DEEPFASHION_ALIAS);
        return clothType;
    }

    @BeforeEach
    public void initTest() {
        clothType = createEntity(em);
    }

    @Test
    @Transactional
    public void createClothType() throws Exception {
        int databaseSizeBeforeCreate = clothTypeRepository.findAll().size();

        // Create the ClothType
        ClothTypeDTO clothTypeDTO = clothTypeMapper.toDto(clothType);
        restClothTypeMockMvc.perform(post("/api/cloth-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the ClothType in the database
        List<ClothType> clothTypeList = clothTypeRepository.findAll();
        assertThat(clothTypeList).hasSize(databaseSizeBeforeCreate + 1);
        ClothType testClothType = clothTypeList.get(clothTypeList.size() - 1);
        assertThat(testClothType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClothType.getDeepfashionAlias()).isEqualTo(DEFAULT_DEEPFASHION_ALIAS);
    }

    @Test
    @Transactional
    public void createClothTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clothTypeRepository.findAll().size();

        // Create the ClothType with an existing ID
        clothType.setId(1L);
        ClothTypeDTO clothTypeDTO = clothTypeMapper.toDto(clothType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClothTypeMockMvc.perform(post("/api/cloth-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClothType in the database
        List<ClothType> clothTypeList = clothTypeRepository.findAll();
        assertThat(clothTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllClothTypes() throws Exception {
        // Initialize the database
        clothTypeRepository.saveAndFlush(clothType);

        // Get all the clothTypeList
        restClothTypeMockMvc.perform(get("/api/cloth-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clothType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].deepfashionAlias").value(hasItem(DEFAULT_DEEPFASHION_ALIAS)));
    }
    
    @Test
    @Transactional
    public void getClothType() throws Exception {
        // Initialize the database
        clothTypeRepository.saveAndFlush(clothType);

        // Get the clothType
        restClothTypeMockMvc.perform(get("/api/cloth-types/{id}", clothType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clothType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.deepfashionAlias").value(DEFAULT_DEEPFASHION_ALIAS));
    }

    @Test
    @Transactional
    public void getNonExistingClothType() throws Exception {
        // Get the clothType
        restClothTypeMockMvc.perform(get("/api/cloth-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClothType() throws Exception {
        // Initialize the database
        clothTypeRepository.saveAndFlush(clothType);

        int databaseSizeBeforeUpdate = clothTypeRepository.findAll().size();

        // Update the clothType
        ClothType updatedClothType = clothTypeRepository.findById(clothType.getId()).get();
        // Disconnect from session so that the updates on updatedClothType are not directly saved in db
        em.detach(updatedClothType);
        updatedClothType
            .name(UPDATED_NAME)
            .deepfashionAlias(UPDATED_DEEPFASHION_ALIAS);
        ClothTypeDTO clothTypeDTO = clothTypeMapper.toDto(updatedClothType);

        restClothTypeMockMvc.perform(put("/api/cloth-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothTypeDTO)))
            .andExpect(status().isOk());

        // Validate the ClothType in the database
        List<ClothType> clothTypeList = clothTypeRepository.findAll();
        assertThat(clothTypeList).hasSize(databaseSizeBeforeUpdate);
        ClothType testClothType = clothTypeList.get(clothTypeList.size() - 1);
        assertThat(testClothType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClothType.getDeepfashionAlias()).isEqualTo(UPDATED_DEEPFASHION_ALIAS);
    }

    @Test
    @Transactional
    public void updateNonExistingClothType() throws Exception {
        int databaseSizeBeforeUpdate = clothTypeRepository.findAll().size();

        // Create the ClothType
        ClothTypeDTO clothTypeDTO = clothTypeMapper.toDto(clothType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClothTypeMockMvc.perform(put("/api/cloth-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClothType in the database
        List<ClothType> clothTypeList = clothTypeRepository.findAll();
        assertThat(clothTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClothType() throws Exception {
        // Initialize the database
        clothTypeRepository.saveAndFlush(clothType);

        int databaseSizeBeforeDelete = clothTypeRepository.findAll().size();

        // Delete the clothType
        restClothTypeMockMvc.perform(delete("/api/cloth-types/{id}", clothType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClothType> clothTypeList = clothTypeRepository.findAll();
        assertThat(clothTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
