package com.mdev.styles.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mdev.styles.web.rest.TestUtil;

public class WardrobeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Wardrobe.class);
        Wardrobe wardrobe1 = new Wardrobe();
        wardrobe1.setId(1L);
        Wardrobe wardrobe2 = new Wardrobe();
        wardrobe2.setId(wardrobe1.getId());
        assertThat(wardrobe1).isEqualTo(wardrobe2);
        wardrobe2.setId(2L);
        assertThat(wardrobe1).isNotEqualTo(wardrobe2);
        wardrobe1.setId(null);
        assertThat(wardrobe1).isNotEqualTo(wardrobe2);
    }
}
