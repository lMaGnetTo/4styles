package com.mdev.styles.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mdev.styles.web.rest.TestUtil;

public class ClothTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClothType.class);
        ClothType clothType1 = new ClothType();
        clothType1.setId(1L);
        ClothType clothType2 = new ClothType();
        clothType2.setId(clothType1.getId());
        assertThat(clothType1).isEqualTo(clothType2);
        clothType2.setId(2L);
        assertThat(clothType1).isNotEqualTo(clothType2);
        clothType1.setId(null);
        assertThat(clothType1).isNotEqualTo(clothType2);
    }
}
