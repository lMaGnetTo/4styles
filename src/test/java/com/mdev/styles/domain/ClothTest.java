package com.mdev.styles.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mdev.styles.web.rest.TestUtil;

public class ClothTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cloth.class);
        Cloth cloth1 = new Cloth();
        cloth1.setId(1L);
        Cloth cloth2 = new Cloth();
        cloth2.setId(cloth1.getId());
        assertThat(cloth1).isEqualTo(cloth2);
        cloth2.setId(2L);
        assertThat(cloth1).isNotEqualTo(cloth2);
        cloth1.setId(null);
        assertThat(cloth1).isNotEqualTo(cloth2);
    }
}
