package com.mdev.styles.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ClothTypeMapperTest {

    private ClothTypeMapper clothTypeMapper;

    @BeforeEach
    public void setUp() {
        clothTypeMapper = new ClothTypeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(clothTypeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(clothTypeMapper.fromId(null)).isNull();
    }
}
