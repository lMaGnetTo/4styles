package com.mdev.styles.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class StyleMapperTest {

    private StyleMapper styleMapper;

    @BeforeEach
    public void setUp() {
        styleMapper = new StyleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(styleMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(styleMapper.fromId(null)).isNull();
    }
}
