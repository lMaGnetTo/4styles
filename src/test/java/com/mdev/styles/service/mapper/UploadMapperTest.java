package com.mdev.styles.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class UploadMapperTest {

    private UploadMapper uploadMapper;

    @BeforeEach
    public void setUp() {
        uploadMapper = new UploadMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(uploadMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uploadMapper.fromId(null)).isNull();
    }
}
