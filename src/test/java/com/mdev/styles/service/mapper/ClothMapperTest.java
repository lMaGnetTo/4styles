package com.mdev.styles.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ClothMapperTest {

    private ClothMapper clothMapper;

    @BeforeEach
    public void setUp() {
        clothMapper = new ClothMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(clothMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(clothMapper.fromId(null)).isNull();
    }
}
