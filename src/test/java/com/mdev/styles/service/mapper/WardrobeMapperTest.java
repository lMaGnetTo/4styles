package com.mdev.styles.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class WardrobeMapperTest {

    private WardrobeMapper wardrobeMapper;

    @BeforeEach
    public void setUp() {
        wardrobeMapper = new WardrobeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(wardrobeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(wardrobeMapper.fromId(null)).isNull();
    }
}
