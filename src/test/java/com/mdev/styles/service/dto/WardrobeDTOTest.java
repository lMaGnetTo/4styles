package com.mdev.styles.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mdev.styles.web.rest.TestUtil;

public class WardrobeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WardrobeDTO.class);
        WardrobeDTO wardrobeDTO1 = new WardrobeDTO();
        wardrobeDTO1.setId(1L);
        WardrobeDTO wardrobeDTO2 = new WardrobeDTO();
        assertThat(wardrobeDTO1).isNotEqualTo(wardrobeDTO2);
        wardrobeDTO2.setId(wardrobeDTO1.getId());
        assertThat(wardrobeDTO1).isEqualTo(wardrobeDTO2);
        wardrobeDTO2.setId(2L);
        assertThat(wardrobeDTO1).isNotEqualTo(wardrobeDTO2);
        wardrobeDTO1.setId(null);
        assertThat(wardrobeDTO1).isNotEqualTo(wardrobeDTO2);
    }
}
