package com.mdev.styles.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mdev.styles.web.rest.TestUtil;

public class ClothTypeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClothTypeDTO.class);
        ClothTypeDTO clothTypeDTO1 = new ClothTypeDTO();
        clothTypeDTO1.setId(1L);
        ClothTypeDTO clothTypeDTO2 = new ClothTypeDTO();
        assertThat(clothTypeDTO1).isNotEqualTo(clothTypeDTO2);
        clothTypeDTO2.setId(clothTypeDTO1.getId());
        assertThat(clothTypeDTO1).isEqualTo(clothTypeDTO2);
        clothTypeDTO2.setId(2L);
        assertThat(clothTypeDTO1).isNotEqualTo(clothTypeDTO2);
        clothTypeDTO1.setId(null);
        assertThat(clothTypeDTO1).isNotEqualTo(clothTypeDTO2);
    }
}
