import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { ClothComponent } from 'app/entities/cloth/cloth.component';
import { ClothService } from 'app/entities/cloth/cloth.service';
import { Cloth } from 'app/shared/model/cloth.model';

describe('Component Tests', () => {
  describe('Cloth Management Component', () => {
    let comp: ClothComponent;
    let fixture: ComponentFixture<ClothComponent>;
    let service: ClothService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothComponent],
        providers: []
      })
        .overrideTemplate(ClothComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClothComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClothService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Cloth(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cloths[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
