import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { ClothDetailComponent } from 'app/entities/cloth/cloth-detail.component';
import { Cloth } from 'app/shared/model/cloth.model';

describe('Component Tests', () => {
  describe('Cloth Management Detail Component', () => {
    let comp: ClothDetailComponent;
    let fixture: ComponentFixture<ClothDetailComponent>;
    const route = ({ data: of({ cloth: new Cloth(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ClothDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ClothDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cloth).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
