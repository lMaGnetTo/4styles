import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { ClothUpdateComponent } from 'app/entities/cloth/cloth-update.component';
import { ClothService } from 'app/entities/cloth/cloth.service';
import { Cloth } from 'app/shared/model/cloth.model';

describe('Component Tests', () => {
  describe('Cloth Management Update Component', () => {
    let comp: ClothUpdateComponent;
    let fixture: ComponentFixture<ClothUpdateComponent>;
    let service: ClothService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ClothUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClothUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClothService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Cloth(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Cloth();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
