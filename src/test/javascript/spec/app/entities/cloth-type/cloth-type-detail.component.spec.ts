import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { ClothTypeDetailComponent } from 'app/entities/cloth-type/cloth-type-detail.component';
import { ClothType } from 'app/shared/model/cloth-type.model';

describe('Component Tests', () => {
  describe('ClothType Management Detail Component', () => {
    let comp: ClothTypeDetailComponent;
    let fixture: ComponentFixture<ClothTypeDetailComponent>;
    const route = ({ data: of({ clothType: new ClothType(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ClothTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ClothTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.clothType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
