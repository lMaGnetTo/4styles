import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { ClothTypeUpdateComponent } from 'app/entities/cloth-type/cloth-type-update.component';
import { ClothTypeService } from 'app/entities/cloth-type/cloth-type.service';
import { ClothType } from 'app/shared/model/cloth-type.model';

describe('Component Tests', () => {
  describe('ClothType Management Update Component', () => {
    let comp: ClothTypeUpdateComponent;
    let fixture: ComponentFixture<ClothTypeUpdateComponent>;
    let service: ClothTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothTypeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ClothTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClothTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClothTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ClothType(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ClothType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
