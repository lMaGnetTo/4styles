import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { ClothTypeComponent } from 'app/entities/cloth-type/cloth-type.component';
import { ClothTypeService } from 'app/entities/cloth-type/cloth-type.service';
import { ClothType } from 'app/shared/model/cloth-type.model';

describe('Component Tests', () => {
  describe('ClothType Management Component', () => {
    let comp: ClothTypeComponent;
    let fixture: ComponentFixture<ClothTypeComponent>;
    let service: ClothTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [ClothTypeComponent],
        providers: []
      })
        .overrideTemplate(ClothTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClothTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClothTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ClothType(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.clothTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
