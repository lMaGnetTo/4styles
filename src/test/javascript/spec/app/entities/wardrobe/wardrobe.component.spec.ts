import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { WardrobeComponent } from 'app/entities/wardrobe/wardrobe.component';
import { WardrobeService } from 'app/entities/wardrobe/wardrobe.service';
import { Wardrobe } from 'app/shared/model/wardrobe.model';

describe('Component Tests', () => {
  describe('Wardrobe Management Component', () => {
    let comp: WardrobeComponent;
    let fixture: ComponentFixture<WardrobeComponent>;
    let service: WardrobeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [WardrobeComponent],
        providers: []
      })
        .overrideTemplate(WardrobeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WardrobeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WardrobeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Wardrobe(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.wardrobes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
