import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { WardrobeUpdateComponent } from 'app/entities/wardrobe/wardrobe-update.component';
import { WardrobeService } from 'app/entities/wardrobe/wardrobe.service';
import { Wardrobe } from 'app/shared/model/wardrobe.model';

describe('Component Tests', () => {
  describe('Wardrobe Management Update Component', () => {
    let comp: WardrobeUpdateComponent;
    let fixture: ComponentFixture<WardrobeUpdateComponent>;
    let service: WardrobeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [WardrobeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(WardrobeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WardrobeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WardrobeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Wardrobe(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Wardrobe();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
