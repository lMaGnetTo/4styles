import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { WardrobeDetailComponent } from 'app/entities/wardrobe/wardrobe-detail.component';
import { Wardrobe } from 'app/shared/model/wardrobe.model';

describe('Component Tests', () => {
  describe('Wardrobe Management Detail Component', () => {
    let comp: WardrobeDetailComponent;
    let fixture: ComponentFixture<WardrobeDetailComponent>;
    const route = ({ data: of({ wardrobe: new Wardrobe(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AppTestModule],
        declarations: [WardrobeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(WardrobeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WardrobeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.wardrobe).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
