import { ICloth } from 'app/shared/model/cloth.model';

export interface IWardrobe {
  id?: number;
  name?: string;
  logoUrl?: string;
  cloths?: ICloth[];
  userId?: number;
}

export class Wardrobe implements IWardrobe {
  constructor(public id?: number, public name?: string, public logoUrl?: string, public cloths?: ICloth[], public userId?: number) {}
}
