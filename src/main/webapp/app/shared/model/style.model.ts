import { Moment } from 'moment';
import { ICloth } from 'app/shared/model/cloth.model';

export interface IStyle {
  id?: number;
  name?: string;
  created?: Moment;
  userId?: number;
  cloths?: ICloth[];
}

export class Style implements IStyle {
  constructor(public id?: number, public name?: string, public created?: Moment, public userId?: number, public cloths?: ICloth[]) {}
}
