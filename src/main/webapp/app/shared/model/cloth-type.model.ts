import { ICloth } from 'app/shared/model/cloth.model';

export interface IClothType {
  id?: number;
  name?: string;
  deepfashionAlias?: string;
  cloths?: ICloth[];
}

export class ClothType implements IClothType {
  constructor(public id?: number, public name?: string, public deepfashionAlias?: string, public cloths?: ICloth[]) {}
}
