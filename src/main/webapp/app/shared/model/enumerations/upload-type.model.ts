export const enum UploadType {
  CLOTH_MAIN = 'CLOTH_MAIN',
  CLOTH_SECONDARY = 'CLOTH_SECONDARY'
}
