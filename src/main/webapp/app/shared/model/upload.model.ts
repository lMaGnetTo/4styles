import { UploadType } from 'app/shared/model/enumerations/upload-type.model';

export interface IUpload {
  id?: number;
  name?: string;
  url?: string;
  type?: UploadType;
  clothId?: number;
}

export class Upload implements IUpload {
  constructor(public id?: number, public name?: string, public url?: string, public type?: UploadType, public clothId?: number) {}
}
