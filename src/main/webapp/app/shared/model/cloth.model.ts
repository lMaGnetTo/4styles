import { IUpload } from 'app/shared/model/upload.model';
import { IStyle } from 'app/shared/model/style.model';

export interface ICloth {
  id?: number;
  name?: string;
  favourite?: boolean;
  available?: boolean;
  imageContentType?: string;
  image?: any;
  uploads?: IUpload[];
  wardrobeName?: string;
  wardrobeId?: number;
  clothTypeId?: number;
  clothTypeName?: string;
  styles?: IStyle[];
}

export class Cloth implements ICloth {
  constructor(
    public id?: number,
    public name?: string,
    public favourite?: boolean,
    public available?: boolean,
    public imageContentType?: string,
    public image?: any,
    public uploads?: IUpload[],
    public wardrobeName?: string,
    public wardrobeId?: number,
    public clothTypeId?: number,
    public clothTypeName?: string,
    public styles?: IStyle[]
  ) {
    this.favourite = this.favourite || false;
    this.available = this.available || false;
  }
}
