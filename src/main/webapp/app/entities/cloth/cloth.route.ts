import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cloth } from 'app/shared/model/cloth.model';
import { ClothService } from './cloth.service';
import { ClothComponent } from './cloth.component';
import { ClothDetailComponent } from './cloth-detail.component';
import { ClothUpdateComponent } from './cloth-update.component';
import { ICloth } from 'app/shared/model/cloth.model';

@Injectable({ providedIn: 'root' })
export class ClothResolve implements Resolve<ICloth> {
  constructor(private service: ClothService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICloth> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((cloth: HttpResponse<Cloth>) => cloth.body));
    }
    return of(new Cloth());
  }
}

export const clothRoute: Routes = [
  {
    path: '',
    component: ClothComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Cloths'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ClothDetailComponent,
    resolve: {
      cloth: ClothResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Cloths'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ClothUpdateComponent,
    resolve: {
      cloth: ClothResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Cloths'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ClothUpdateComponent,
    resolve: {
      cloth: ClothResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Cloths'
    },
    canActivate: [UserRouteAccessService]
  }
];
