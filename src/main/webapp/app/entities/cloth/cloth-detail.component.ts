import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { ICloth } from 'app/shared/model/cloth.model';

@Component({
  selector: 'jhi-cloth-detail',
  templateUrl: './cloth-detail.component.html'
})
export class ClothDetailComponent implements OnInit {
  cloth: ICloth;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cloth }) => {
      this.cloth = cloth;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
