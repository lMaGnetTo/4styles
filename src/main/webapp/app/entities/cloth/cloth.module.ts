import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared/shared.module';
import { ClothComponent } from './cloth.component';
import { ClothDetailComponent } from './cloth-detail.component';
import { ClothUpdateComponent } from './cloth-update.component';
import { ClothDeleteDialogComponent } from './cloth-delete-dialog.component';
import { clothRoute } from './cloth.route';

@NgModule({
  imports: [AppSharedModule, RouterModule.forChild(clothRoute)],
  declarations: [ClothComponent, ClothDetailComponent, ClothUpdateComponent, ClothDeleteDialogComponent],
  entryComponents: [ClothDeleteDialogComponent]
})
export class AppClothModule {}
