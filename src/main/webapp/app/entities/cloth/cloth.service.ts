import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICloth } from 'app/shared/model/cloth.model';

type EntityResponseType = HttpResponse<ICloth>;
type EntityArrayResponseType = HttpResponse<ICloth[]>;

@Injectable({ providedIn: 'root' })
export class ClothService {
  public resourceUrl = SERVER_API_URL + 'api/cloths';

  constructor(protected http: HttpClient) {}

  create(cloth: ICloth): Observable<EntityResponseType> {
    return this.http.post<ICloth>(this.resourceUrl, cloth, { observe: 'response' });
  }

  update(cloth: ICloth): Observable<EntityResponseType> {
    return this.http.put<ICloth>(this.resourceUrl, cloth, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICloth>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICloth[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
