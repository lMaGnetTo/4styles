import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICloth } from 'app/shared/model/cloth.model';
import { ClothService } from './cloth.service';

@Component({
  templateUrl: './cloth-delete-dialog.component.html'
})
export class ClothDeleteDialogComponent {
  cloth: ICloth;

  constructor(protected clothService: ClothService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.clothService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'clothListModification',
        content: 'Deleted an cloth'
      });
      this.activeModal.dismiss(true);
    });
  }
}
