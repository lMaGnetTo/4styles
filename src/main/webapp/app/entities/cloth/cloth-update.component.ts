import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ICloth, Cloth } from 'app/shared/model/cloth.model';
import { ClothService } from './cloth.service';
import { IWardrobe } from 'app/shared/model/wardrobe.model';
import { WardrobeService } from 'app/entities/wardrobe/wardrobe.service';
import { IClothType } from 'app/shared/model/cloth-type.model';
import { ClothTypeService } from 'app/entities/cloth-type/cloth-type.service';
import { IStyle } from 'app/shared/model/style.model';
import { StyleService } from 'app/entities/style/style.service';

@Component({
  selector: 'jhi-cloth-update',
  templateUrl: './cloth-update.component.html'
})
export class ClothUpdateComponent implements OnInit {
  isSaving: boolean;

  wardrobes: IWardrobe[];

  clothtypes: IClothType[];

  styles: IStyle[];

  editForm = this.fb.group({
    id: [],
    name: [],
    favourite: [],
    available: [],
    image: [],
    imageContentType: [],
    wardrobeId: [],
    clothTypeId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected clothService: ClothService,
    protected wardrobeService: WardrobeService,
    protected clothTypeService: ClothTypeService,
    protected styleService: StyleService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cloth }) => {
      this.updateForm(cloth);
    });
    this.wardrobeService
      .query()
      .subscribe((res: HttpResponse<IWardrobe[]>) => (this.wardrobes = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.clothTypeService
      .query()
      .subscribe((res: HttpResponse<IClothType[]>) => (this.clothtypes = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.styleService
      .query()
      .subscribe((res: HttpResponse<IStyle[]>) => (this.styles = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(cloth: ICloth) {
    this.editForm.patchValue({
      id: cloth.id,
      name: cloth.name,
      favourite: cloth.favourite,
      available: cloth.available,
      image: cloth.image,
      imageContentType: cloth.imageContentType,
      wardrobeId: cloth.wardrobeId,
      clothTypeId: cloth.clothTypeId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cloth = this.createFromForm();
    if (cloth.id !== undefined) {
      this.subscribeToSaveResponse(this.clothService.update(cloth));
    } else {
      this.subscribeToSaveResponse(this.clothService.create(cloth));
    }
  }

  private createFromForm(): ICloth {
    return {
      ...new Cloth(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      favourite: this.editForm.get(['favourite']).value,
      available: this.editForm.get(['available']).value,
      imageContentType: this.editForm.get(['imageContentType']).value,
      image: this.editForm.get(['image']).value,
      wardrobeId: this.editForm.get(['wardrobeId']).value,
      clothTypeId: this.editForm.get(['clothTypeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICloth>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackWardrobeById(index: number, item: IWardrobe) {
    return item.id;
  }

  trackClothTypeById(index: number, item: IClothType) {
    return item.id;
  }

  trackStyleById(index: number, item: IStyle) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
