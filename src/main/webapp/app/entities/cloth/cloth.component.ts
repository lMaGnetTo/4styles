import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICloth } from 'app/shared/model/cloth.model';
import { ClothService } from './cloth.service';
import { ClothDeleteDialogComponent } from './cloth-delete-dialog.component';

@Component({
  selector: 'jhi-cloth',
  templateUrl: './cloth.component.html'
})
export class ClothComponent implements OnInit, OnDestroy {
  cloths: ICloth[];
  eventSubscriber: Subscription;

  constructor(
    protected clothService: ClothService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll() {
    this.clothService.query().subscribe((res: HttpResponse<ICloth[]>) => {
      this.cloths = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInCloths();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICloth) {
    return item.id;
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  registerChangeInCloths() {
    this.eventSubscriber = this.eventManager.subscribe('clothListModification', () => this.loadAll());
  }

  delete(cloth: ICloth) {
    const modalRef = this.modalService.open(ClothDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cloth = cloth;
  }
}
