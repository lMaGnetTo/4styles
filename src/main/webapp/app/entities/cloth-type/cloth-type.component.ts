import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IClothType } from 'app/shared/model/cloth-type.model';
import { ClothTypeService } from './cloth-type.service';
import { ClothTypeDeleteDialogComponent } from './cloth-type-delete-dialog.component';

@Component({
  selector: 'jhi-cloth-type',
  templateUrl: './cloth-type.component.html'
})
export class ClothTypeComponent implements OnInit, OnDestroy {
  clothTypes: IClothType[];
  eventSubscriber: Subscription;

  constructor(protected clothTypeService: ClothTypeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.clothTypeService.query().subscribe((res: HttpResponse<IClothType[]>) => {
      this.clothTypes = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInClothTypes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IClothType) {
    return item.id;
  }

  registerChangeInClothTypes() {
    this.eventSubscriber = this.eventManager.subscribe('clothTypeListModification', () => this.loadAll());
  }

  delete(clothType: IClothType) {
    const modalRef = this.modalService.open(ClothTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.clothType = clothType;
  }
}
