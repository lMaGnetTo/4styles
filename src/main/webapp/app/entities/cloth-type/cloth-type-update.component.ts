import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IClothType, ClothType } from 'app/shared/model/cloth-type.model';
import { ClothTypeService } from './cloth-type.service';

@Component({
  selector: 'jhi-cloth-type-update',
  templateUrl: './cloth-type-update.component.html'
})
export class ClothTypeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    deepfashionAlias: []
  });

  constructor(protected clothTypeService: ClothTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ clothType }) => {
      this.updateForm(clothType);
    });
  }

  updateForm(clothType: IClothType) {
    this.editForm.patchValue({
      id: clothType.id,
      name: clothType.name,
      deepfashionAlias: clothType.deepfashionAlias
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const clothType = this.createFromForm();
    if (clothType.id !== undefined) {
      this.subscribeToSaveResponse(this.clothTypeService.update(clothType));
    } else {
      this.subscribeToSaveResponse(this.clothTypeService.create(clothType));
    }
  }

  private createFromForm(): IClothType {
    return {
      ...new ClothType(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      deepfashionAlias: this.editForm.get(['deepfashionAlias']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClothType>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
