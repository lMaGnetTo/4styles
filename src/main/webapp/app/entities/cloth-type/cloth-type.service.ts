import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IClothType } from 'app/shared/model/cloth-type.model';

type EntityResponseType = HttpResponse<IClothType>;
type EntityArrayResponseType = HttpResponse<IClothType[]>;

@Injectable({ providedIn: 'root' })
export class ClothTypeService {
  public resourceUrl = SERVER_API_URL + 'api/cloth-types';

  constructor(protected http: HttpClient) {}

  create(clothType: IClothType): Observable<EntityResponseType> {
    return this.http.post<IClothType>(this.resourceUrl, clothType, { observe: 'response' });
  }

  update(clothType: IClothType): Observable<EntityResponseType> {
    return this.http.put<IClothType>(this.resourceUrl, clothType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IClothType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IClothType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
