import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared/shared.module';
import { ClothTypeComponent } from './cloth-type.component';
import { ClothTypeDetailComponent } from './cloth-type-detail.component';
import { ClothTypeUpdateComponent } from './cloth-type-update.component';
import { ClothTypeDeleteDialogComponent } from './cloth-type-delete-dialog.component';
import { clothTypeRoute } from './cloth-type.route';

@NgModule({
  imports: [AppSharedModule, RouterModule.forChild(clothTypeRoute)],
  declarations: [ClothTypeComponent, ClothTypeDetailComponent, ClothTypeUpdateComponent, ClothTypeDeleteDialogComponent],
  entryComponents: [ClothTypeDeleteDialogComponent]
})
export class AppClothTypeModule {}
