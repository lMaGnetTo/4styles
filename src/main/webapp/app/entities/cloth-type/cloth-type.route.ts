import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClothType } from 'app/shared/model/cloth-type.model';
import { ClothTypeService } from './cloth-type.service';
import { ClothTypeComponent } from './cloth-type.component';
import { ClothTypeDetailComponent } from './cloth-type-detail.component';
import { ClothTypeUpdateComponent } from './cloth-type-update.component';
import { IClothType } from 'app/shared/model/cloth-type.model';

@Injectable({ providedIn: 'root' })
export class ClothTypeResolve implements Resolve<IClothType> {
  constructor(private service: ClothTypeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IClothType> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((clothType: HttpResponse<ClothType>) => clothType.body));
    }
    return of(new ClothType());
  }
}

export const clothTypeRoute: Routes = [
  {
    path: '',
    component: ClothTypeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ClothTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ClothTypeDetailComponent,
    resolve: {
      clothType: ClothTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ClothTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ClothTypeUpdateComponent,
    resolve: {
      clothType: ClothTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ClothTypes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ClothTypeUpdateComponent,
    resolve: {
      clothType: ClothTypeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ClothTypes'
    },
    canActivate: [UserRouteAccessService]
  }
];
