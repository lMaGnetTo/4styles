import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IClothType } from 'app/shared/model/cloth-type.model';

@Component({
  selector: 'jhi-cloth-type-detail',
  templateUrl: './cloth-type-detail.component.html'
})
export class ClothTypeDetailComponent implements OnInit {
  clothType: IClothType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ clothType }) => {
      this.clothType = clothType;
    });
  }

  previousState() {
    window.history.back();
  }
}
