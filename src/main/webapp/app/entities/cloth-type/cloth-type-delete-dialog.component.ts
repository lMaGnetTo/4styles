import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IClothType } from 'app/shared/model/cloth-type.model';
import { ClothTypeService } from './cloth-type.service';

@Component({
  templateUrl: './cloth-type-delete-dialog.component.html'
})
export class ClothTypeDeleteDialogComponent {
  clothType: IClothType;

  constructor(protected clothTypeService: ClothTypeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.clothTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'clothTypeListModification',
        content: 'Deleted an clothType'
      });
      this.activeModal.dismiss(true);
    });
  }
}
