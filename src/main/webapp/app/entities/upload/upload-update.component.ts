import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IUpload, Upload } from 'app/shared/model/upload.model';
import { UploadService } from './upload.service';
import { ICloth } from 'app/shared/model/cloth.model';
import { ClothService } from 'app/entities/cloth/cloth.service';

@Component({
  selector: 'jhi-upload-update',
  templateUrl: './upload-update.component.html'
})
export class UploadUpdateComponent implements OnInit {
  isSaving: boolean;

  cloths: ICloth[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    url: [],
    type: [],
    clothId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected uploadService: UploadService,
    protected clothService: ClothService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ upload }) => {
      this.updateForm(upload);
    });
    this.clothService
      .query()
      .subscribe((res: HttpResponse<ICloth[]>) => (this.cloths = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(upload: IUpload) {
    this.editForm.patchValue({
      id: upload.id,
      name: upload.name,
      url: upload.url,
      type: upload.type,
      clothId: upload.clothId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const upload = this.createFromForm();
    if (upload.id !== undefined) {
      this.subscribeToSaveResponse(this.uploadService.update(upload));
    } else {
      this.subscribeToSaveResponse(this.uploadService.create(upload));
    }
  }

  private createFromForm(): IUpload {
    return {
      ...new Upload(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      url: this.editForm.get(['url']).value,
      type: this.editForm.get(['type']).value,
      clothId: this.editForm.get(['clothId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUpload>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackClothById(index: number, item: ICloth) {
    return item.id;
  }
}
