import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUpload } from 'app/shared/model/upload.model';
import { UploadService } from './upload.service';
import { UploadDeleteDialogComponent } from './upload-delete-dialog.component';

@Component({
  selector: 'jhi-upload',
  templateUrl: './upload.component.html'
})
export class UploadComponent implements OnInit, OnDestroy {
  uploads: IUpload[];
  eventSubscriber: Subscription;

  constructor(protected uploadService: UploadService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.uploadService.query().subscribe((res: HttpResponse<IUpload[]>) => {
      this.uploads = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInUploads();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IUpload) {
    return item.id;
  }

  registerChangeInUploads() {
    this.eventSubscriber = this.eventManager.subscribe('uploadListModification', () => this.loadAll());
  }

  delete(upload: IUpload) {
    const modalRef = this.modalService.open(UploadDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.upload = upload;
  }
}
