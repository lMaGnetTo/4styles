import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Upload } from 'app/shared/model/upload.model';
import { UploadService } from './upload.service';
import { UploadComponent } from './upload.component';
import { UploadDetailComponent } from './upload-detail.component';
import { UploadUpdateComponent } from './upload-update.component';
import { IUpload } from 'app/shared/model/upload.model';

@Injectable({ providedIn: 'root' })
export class UploadResolve implements Resolve<IUpload> {
  constructor(private service: UploadService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUpload> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((upload: HttpResponse<Upload>) => upload.body));
    }
    return of(new Upload());
  }
}

export const uploadRoute: Routes = [
  {
    path: '',
    component: UploadComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Uploads'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UploadDetailComponent,
    resolve: {
      upload: UploadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Uploads'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UploadUpdateComponent,
    resolve: {
      upload: UploadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Uploads'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UploadUpdateComponent,
    resolve: {
      upload: UploadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Uploads'
    },
    canActivate: [UserRouteAccessService]
  }
];
