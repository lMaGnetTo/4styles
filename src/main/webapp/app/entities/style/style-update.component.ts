import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IStyle, Style } from 'app/shared/model/style.model';
import { StyleService } from './style.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ICloth } from 'app/shared/model/cloth.model';
import { ClothService } from 'app/entities/cloth/cloth.service';

@Component({
  selector: 'jhi-style-update',
  templateUrl: './style-update.component.html'
})
export class StyleUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  cloths: ICloth[];

  editForm = this.fb.group({
    id: [],
    name: [],
    created: [],
    userId: [],
    cloths: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected styleService: StyleService,
    protected userService: UserService,
    protected clothService: ClothService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ style }) => {
      this.updateForm(style);
    });
    this.userService
      .query()
      .subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.clothService
      .query()
      .subscribe((res: HttpResponse<ICloth[]>) => (this.cloths = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(style: IStyle) {
    this.editForm.patchValue({
      id: style.id,
      name: style.name,
      created: style.created != null ? style.created.format(DATE_TIME_FORMAT) : null,
      userId: style.userId,
      cloths: style.cloths
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const style = this.createFromForm();
    if (style.id !== undefined) {
      this.subscribeToSaveResponse(this.styleService.update(style));
    } else {
      this.subscribeToSaveResponse(this.styleService.create(style));
    }
  }

  private createFromForm(): IStyle {
    return {
      ...new Style(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      userId: this.editForm.get(['userId']).value,
      cloths: this.editForm.get(['cloths']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStyle>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackClothById(index: number, item: ICloth) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
