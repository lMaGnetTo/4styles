import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStyle } from 'app/shared/model/style.model';
import { StyleService } from './style.service';

@Component({
  templateUrl: './style-delete-dialog.component.html'
})
export class StyleDeleteDialogComponent {
  style: IStyle;

  constructor(protected styleService: StyleService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.styleService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'styleListModification',
        content: 'Deleted an style'
      });
      this.activeModal.dismiss(true);
    });
  }
}
