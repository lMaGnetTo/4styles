import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStyle } from 'app/shared/model/style.model';
import { StyleService } from './style.service';
import { StyleDeleteDialogComponent } from './style-delete-dialog.component';

@Component({
  selector: 'jhi-style',
  templateUrl: './style.component.html'
})
export class StyleComponent implements OnInit, OnDestroy {
  styles: IStyle[];
  eventSubscriber: Subscription;

  constructor(protected styleService: StyleService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.styleService.query().subscribe((res: HttpResponse<IStyle[]>) => {
      this.styles = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInStyles();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IStyle) {
    return item.id;
  }

  registerChangeInStyles() {
    this.eventSubscriber = this.eventManager.subscribe('styleListModification', () => this.loadAll());
  }

  delete(style: IStyle) {
    const modalRef = this.modalService.open(StyleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.style = style;
  }
}
