import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared/shared.module';
import { StyleComponent } from './style.component';
import { StyleDetailComponent } from './style-detail.component';
import { StyleUpdateComponent } from './style-update.component';
import { StyleDeleteDialogComponent } from './style-delete-dialog.component';
import { styleRoute } from './style.route';

@NgModule({
  imports: [AppSharedModule, RouterModule.forChild(styleRoute)],
  declarations: [StyleComponent, StyleDetailComponent, StyleUpdateComponent, StyleDeleteDialogComponent],
  entryComponents: [StyleDeleteDialogComponent]
})
export class AppStyleModule {}
