import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStyle } from 'app/shared/model/style.model';

type EntityResponseType = HttpResponse<IStyle>;
type EntityArrayResponseType = HttpResponse<IStyle[]>;

@Injectable({ providedIn: 'root' })
export class StyleService {
  public resourceUrl = SERVER_API_URL + 'api/styles';

  constructor(protected http: HttpClient) {}

  create(style: IStyle): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(style);
    return this.http
      .post<IStyle>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(style: IStyle): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(style);
    return this.http
      .put<IStyle>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStyle>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStyle[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(style: IStyle): IStyle {
    const copy: IStyle = Object.assign({}, style, {
      created: style.created != null && style.created.isValid() ? style.created.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((style: IStyle) => {
        style.created = style.created != null ? moment(style.created) : null;
      });
    }
    return res;
  }
}
