import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Style } from 'app/shared/model/style.model';
import { StyleService } from './style.service';
import { StyleComponent } from './style.component';
import { StyleDetailComponent } from './style-detail.component';
import { StyleUpdateComponent } from './style-update.component';
import { IStyle } from 'app/shared/model/style.model';

@Injectable({ providedIn: 'root' })
export class StyleResolve implements Resolve<IStyle> {
  constructor(private service: StyleService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStyle> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((style: HttpResponse<Style>) => style.body));
    }
    return of(new Style());
  }
}

export const styleRoute: Routes = [
  {
    path: '',
    component: StyleComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Styles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StyleDetailComponent,
    resolve: {
      style: StyleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Styles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StyleUpdateComponent,
    resolve: {
      style: StyleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Styles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StyleUpdateComponent,
    resolve: {
      style: StyleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Styles'
    },
    canActivate: [UserRouteAccessService]
  }
];
