import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared/shared.module';
import { WardrobeComponent } from './wardrobe.component';
import { WardrobeDetailComponent } from './wardrobe-detail.component';
import { WardrobeUpdateComponent } from './wardrobe-update.component';
import { WardrobeDeleteDialogComponent } from './wardrobe-delete-dialog.component';
import { wardrobeRoute } from './wardrobe.route';

@NgModule({
  imports: [AppSharedModule, RouterModule.forChild(wardrobeRoute)],
  declarations: [WardrobeComponent, WardrobeDetailComponent, WardrobeUpdateComponent, WardrobeDeleteDialogComponent],
  entryComponents: [WardrobeDeleteDialogComponent]
})
export class AppWardrobeModule {}
