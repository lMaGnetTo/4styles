import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Wardrobe } from 'app/shared/model/wardrobe.model';
import { WardrobeService } from './wardrobe.service';
import { WardrobeComponent } from './wardrobe.component';
import { WardrobeDetailComponent } from './wardrobe-detail.component';
import { WardrobeUpdateComponent } from './wardrobe-update.component';
import { IWardrobe } from 'app/shared/model/wardrobe.model';

@Injectable({ providedIn: 'root' })
export class WardrobeResolve implements Resolve<IWardrobe> {
  constructor(private service: WardrobeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWardrobe> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((wardrobe: HttpResponse<Wardrobe>) => wardrobe.body));
    }
    return of(new Wardrobe());
  }
}

export const wardrobeRoute: Routes = [
  {
    path: '',
    component: WardrobeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Wardrobes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WardrobeDetailComponent,
    resolve: {
      wardrobe: WardrobeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Wardrobes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WardrobeUpdateComponent,
    resolve: {
      wardrobe: WardrobeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Wardrobes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WardrobeUpdateComponent,
    resolve: {
      wardrobe: WardrobeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Wardrobes'
    },
    canActivate: [UserRouteAccessService]
  }
];
