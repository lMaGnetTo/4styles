import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWardrobe } from 'app/shared/model/wardrobe.model';

@Component({
  selector: 'jhi-wardrobe-detail',
  templateUrl: './wardrobe-detail.component.html'
})
export class WardrobeDetailComponent implements OnInit {
  wardrobe: IWardrobe;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ wardrobe }) => {
      this.wardrobe = wardrobe;
    });
  }

  previousState() {
    window.history.back();
  }
}
