import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IWardrobe, Wardrobe } from 'app/shared/model/wardrobe.model';
import { WardrobeService } from './wardrobe.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-wardrobe-update',
  templateUrl: './wardrobe-update.component.html'
})
export class WardrobeUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    name: [],
    logoUrl: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected wardrobeService: WardrobeService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ wardrobe }) => {
      this.updateForm(wardrobe);
    });
    this.userService
      .query()
      .subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(wardrobe: IWardrobe) {
    this.editForm.patchValue({
      id: wardrobe.id,
      name: wardrobe.name,
      logoUrl: wardrobe.logoUrl,
      userId: wardrobe.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const wardrobe = this.createFromForm();
    if (wardrobe.id !== undefined) {
      this.subscribeToSaveResponse(this.wardrobeService.update(wardrobe));
    } else {
      this.subscribeToSaveResponse(this.wardrobeService.create(wardrobe));
    }
  }

  private createFromForm(): IWardrobe {
    return {
      ...new Wardrobe(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      logoUrl: this.editForm.get(['logoUrl']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWardrobe>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
