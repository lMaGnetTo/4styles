import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWardrobe } from 'app/shared/model/wardrobe.model';
import { WardrobeService } from './wardrobe.service';
import { WardrobeDeleteDialogComponent } from './wardrobe-delete-dialog.component';

@Component({
  selector: 'jhi-wardrobe',
  templateUrl: './wardrobe.component.html'
})
export class WardrobeComponent implements OnInit, OnDestroy {
  wardrobes: IWardrobe[];
  eventSubscriber: Subscription;

  constructor(protected wardrobeService: WardrobeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.wardrobeService.query().subscribe((res: HttpResponse<IWardrobe[]>) => {
      this.wardrobes = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInWardrobes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IWardrobe) {
    return item.id;
  }

  registerChangeInWardrobes() {
    this.eventSubscriber = this.eventManager.subscribe('wardrobeListModification', () => this.loadAll());
  }

  delete(wardrobe: IWardrobe) {
    const modalRef = this.modalService.open(WardrobeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.wardrobe = wardrobe;
  }
}
