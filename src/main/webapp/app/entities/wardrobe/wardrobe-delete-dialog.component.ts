import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWardrobe } from 'app/shared/model/wardrobe.model';
import { WardrobeService } from './wardrobe.service';

@Component({
  templateUrl: './wardrobe-delete-dialog.component.html'
})
export class WardrobeDeleteDialogComponent {
  wardrobe: IWardrobe;

  constructor(protected wardrobeService: WardrobeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.wardrobeService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'wardrobeListModification',
        content: 'Deleted an wardrobe'
      });
      this.activeModal.dismiss(true);
    });
  }
}
