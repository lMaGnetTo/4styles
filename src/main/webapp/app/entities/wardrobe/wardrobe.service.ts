import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWardrobe } from 'app/shared/model/wardrobe.model';

type EntityResponseType = HttpResponse<IWardrobe>;
type EntityArrayResponseType = HttpResponse<IWardrobe[]>;

@Injectable({ providedIn: 'root' })
export class WardrobeService {
  public resourceUrl = SERVER_API_URL + 'api/wardrobes';

  constructor(protected http: HttpClient) {}

  create(wardrobe: IWardrobe): Observable<EntityResponseType> {
    return this.http.post<IWardrobe>(this.resourceUrl, wardrobe, { observe: 'response' });
  }

  update(wardrobe: IWardrobe): Observable<EntityResponseType> {
    return this.http.put<IWardrobe>(this.resourceUrl, wardrobe, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWardrobe>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWardrobe[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
