import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'upload',
        loadChildren: () => import('./upload/upload.module').then(m => m.AppUploadModule)
      },
      {
        path: 'wardrobe',
        loadChildren: () => import('./wardrobe/wardrobe.module').then(m => m.AppWardrobeModule)
      },
      {
        path: 'cloth',
        loadChildren: () => import('./cloth/cloth.module').then(m => m.AppClothModule)
      },
      {
        path: 'style',
        loadChildren: () => import('./style/style.module').then(m => m.AppStyleModule)
      },
      {
        path: 'cloth-type',
        loadChildren: () => import('./cloth-type/cloth-type.module').then(m => m.AppClothTypeModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class AppEntityModule {}
