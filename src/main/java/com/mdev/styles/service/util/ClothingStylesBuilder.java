package com.mdev.styles.service.util;

import com.github.javafaker.Faker;
import com.google.common.collect.Lists;
import com.mdev.styles.domain.ClothType;
import com.mdev.styles.service.dto.ClothDTO;
import com.mdev.styles.service.dto.StyleDTO;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClothingStylesBuilder {
    private List<ClothDTO> clothes = new ArrayList<>();
    private int maxStylesCount = 0;
    private Faker faker;

    public ClothingStylesBuilder(List<ClothDTO> clothes, int maxStylesCount) {
        this.clothes = clothes;
        this.maxStylesCount = maxStylesCount;
        this.faker = new Faker();
    }

    private boolean typeContains(ArrayList<Long> typeIds, Long clothTypeId) {
        for (Long typeId : typeIds)
            if (typeId.equals(clothTypeId))
                return true;
        return false;
    }

    private void RecursiveBuild(ArrayList<ArrayList<ClothDTO>> clothesSet, int n, ArrayList<StyleDTO> styles, ArrayList<ClothDTO> addClothes) {
        if (styles.size() >= maxStylesCount)
            return;

        if (n == clothesSet.size()) {
            StyleDTO style = new StyleDTO();
            style.setCreated(Instant.now());
            style.setName(faker.superhero().name());
            for (ClothDTO addClothe : addClothes) style.addClothes(addClothe);
            styles.add(style);
            return;
        }

        for (int i = 0; i < clothesSet.get(n).size(); i++) {
            addClothes.add(clothesSet.get(n).get(i));
            RecursiveBuild(clothesSet, n + 1, styles, addClothes);
            addClothes.remove(clothesSet.get(n).get(i));
        }
    }

    public ArrayList<StyleDTO> buildStyles() {
        ArrayList<Long> typeIds = new ArrayList<>();
        ArrayList<StyleDTO> styles = new ArrayList<>();
        ArrayList<ClothDTO> addClothes = new ArrayList<>();
        ArrayList<ArrayList<ClothDTO>> clothesSet = new ArrayList<>();

        for (ClothDTO clothe : clothes)
            if (!typeContains(typeIds, clothe.getClothTypeId()))
                typeIds.add(clothe.getClothTypeId());

        for (Long typeId : typeIds) {
            ArrayList<ClothDTO> setCloth = new ArrayList<>();
            for (ClothDTO clothe : clothes)
                if (clothe.getClothTypeId().equals(typeId))
                    setCloth.add(clothe);
            clothesSet.add(setCloth);
        }

        RecursiveBuild(clothesSet, 0, styles, addClothes);

        Collections.sort(styles, new Comparator<StyleDTO>() {
            @Override
            public int compare(StyleDTO arg0, StyleDTO arg1) {
                if (arg0.getCloths().size() < arg1.getCloths().size())
                    return 1;
                return -1;
            }
        });

        return styles;
    }
}
