package com.mdev.styles.service.dto;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.mdev.styles.domain.Cloth} entity.
 */
public class ClothDTO implements Serializable {

    private Long id;

    private String name;

    private Boolean favourite;

    private Boolean available;

    @Lob
    private byte[] image;

    private String imageContentType;

    private Long wardrobeId;

    private String wardrobeName;

    private Long clothTypeId;

    private String clothTypeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Long getWardrobeId() {
        return wardrobeId;
    }

    public void setWardrobeId(Long wardrobeId) {
        this.wardrobeId = wardrobeId;
    }

    public String getWardrobeName() {
        return wardrobeName;
    }

    public void setWardrobeName(String wardrobeName) {
        this.wardrobeName = wardrobeName;
    }

    public Long getClothTypeId() {
        return clothTypeId;
    }

    public void setClothTypeId(Long clothTypeId) {
        this.clothTypeId = clothTypeId;
    }

    public String getClothTypeName() {
        return clothTypeName;
    }

    public void setClothTypeName(String clothTypeName) {
        this.clothTypeName = clothTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClothDTO clothDTO = (ClothDTO) o;
        if (clothDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clothDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClothDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", favourite='" + isFavourite() + "'" +
            ", available='" + isAvailable() + "'" +
            ", image='" + getImage() + "'" +
            ", wardrobe=" + getWardrobeId() +
            ", wardrobe='" + getWardrobeName() + "'" +
            ", clothType=" + getClothTypeId() +
            "}";
    }
}
