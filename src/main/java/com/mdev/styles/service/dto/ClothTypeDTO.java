package com.mdev.styles.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mdev.styles.domain.ClothType} entity.
 */
public class ClothTypeDTO implements Serializable {

    private Long id;

    private String name;

    private String deepfashionAlias;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeepfashionAlias() {
        return deepfashionAlias;
    }

    public void setDeepfashionAlias(String deepfashionAlias) {
        this.deepfashionAlias = deepfashionAlias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClothTypeDTO clothTypeDTO = (ClothTypeDTO) o;
        if (clothTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clothTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClothTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", deepfashionAlias='" + getDeepfashionAlias() + "'" +
            "}";
    }
}
