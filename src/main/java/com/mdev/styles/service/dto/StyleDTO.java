package com.mdev.styles.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mdev.styles.domain.Style} entity.
 */
public class StyleDTO implements Serializable {

    private Long id;

    private String name;

    private Instant created;


    private Long userId;

    private Set<ClothDTO> cloths = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<ClothDTO> getCloths() {
        return cloths;
    }

    public void setCloths(Set<ClothDTO> cloths) {
        this.cloths = cloths;
    }

    public Set<ClothDTO> addClothes(ClothDTO cloth) {
        cloths.add(cloth);
        return cloths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StyleDTO styleDTO = (StyleDTO) o;
        if (styleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), styleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StyleDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", created='" + getCreated() + "'" +
            ", user=" + getUserId() +
            "}";
    }
}
