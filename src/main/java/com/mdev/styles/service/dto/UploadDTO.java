package com.mdev.styles.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.mdev.styles.domain.enumeration.UploadType;

/**
 * A DTO for the {@link com.mdev.styles.domain.Upload} entity.
 */
public class UploadDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String url;

    private UploadType type;


    private Long clothId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UploadType getType() {
        return type;
    }

    public void setType(UploadType type) {
        this.type = type;
    }

    public Long getClothId() {
        return clothId;
    }

    public void setClothId(Long clothId) {
        this.clothId = clothId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UploadDTO uploadDTO = (UploadDTO) o;
        if (uploadDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), uploadDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UploadDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", type='" + getType() + "'" +
            ", cloth=" + getClothId() +
            "}";
    }
}
