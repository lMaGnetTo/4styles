package com.mdev.styles.service.mapper;

import com.mdev.styles.domain.*;
import com.mdev.styles.service.dto.ClothDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cloth} and its DTO {@link ClothDTO}.
 */
@Mapper(componentModel = "spring", uses = {WardrobeMapper.class, ClothTypeMapper.class})
public interface ClothMapper extends EntityMapper<ClothDTO, Cloth> {

    @Mapping(source = "wardrobe.id", target = "wardrobeId")
    @Mapping(source = "wardrobe.name", target = "wardrobeName")
    @Mapping(source = "clothType.id", target = "clothTypeId")
    @Mapping(source = "clothType.name", target = "clothTypeName")
    ClothDTO toDto(Cloth cloth);

    @Mapping(target = "uploads", ignore = true)
    @Mapping(target = "removeUploads", ignore = true)
    @Mapping(source = "wardrobeId", target = "wardrobe")
    @Mapping(source = "clothTypeId", target = "clothType")
    @Mapping(target = "styles", ignore = true)
    @Mapping(target = "removeStyles", ignore = true)
    Cloth toEntity(ClothDTO clothDTO);

    default Cloth fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cloth cloth = new Cloth();
        cloth.setId(id);
        return cloth;
    }
}
