package com.mdev.styles.service.mapper;

import com.mdev.styles.domain.*;
import com.mdev.styles.service.dto.UploadDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Upload} and its DTO {@link UploadDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClothMapper.class})
public interface UploadMapper extends EntityMapper<UploadDTO, Upload> {

    @Mapping(source = "cloth.id", target = "clothId")
    UploadDTO toDto(Upload upload);

    @Mapping(source = "clothId", target = "cloth")
    Upload toEntity(UploadDTO uploadDTO);

    default Upload fromId(Long id) {
        if (id == null) {
            return null;
        }
        Upload upload = new Upload();
        upload.setId(id);
        return upload;
    }
}
