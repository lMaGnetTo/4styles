package com.mdev.styles.service.mapper;

import com.mdev.styles.domain.*;
import com.mdev.styles.service.dto.WardrobeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Wardrobe} and its DTO {@link WardrobeDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface WardrobeMapper extends EntityMapper<WardrobeDTO, Wardrobe> {

    @Mapping(source = "user.id", target = "userId")
    WardrobeDTO toDto(Wardrobe wardrobe);

    @Mapping(target = "cloths", ignore = true)
    @Mapping(target = "removeCloth", ignore = true)
    @Mapping(source = "userId", target = "user")
    Wardrobe toEntity(WardrobeDTO wardrobeDTO);

    default Wardrobe fromId(Long id) {
        if (id == null) {
            return null;
        }
        Wardrobe wardrobe = new Wardrobe();
        wardrobe.setId(id);
        return wardrobe;
    }
}
