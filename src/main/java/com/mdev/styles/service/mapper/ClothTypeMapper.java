package com.mdev.styles.service.mapper;

import com.mdev.styles.domain.*;
import com.mdev.styles.service.dto.ClothTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClothType} and its DTO {@link ClothTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClothTypeMapper extends EntityMapper<ClothTypeDTO, ClothType> {


    @Mapping(target = "cloths", ignore = true)
    @Mapping(target = "removeCloths", ignore = true)
    ClothType toEntity(ClothTypeDTO clothTypeDTO);

    default ClothType fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClothType clothType = new ClothType();
        clothType.setId(id);
        return clothType;
    }
}
