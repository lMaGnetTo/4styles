package com.mdev.styles.service.mapper;

import com.mdev.styles.domain.*;
import com.mdev.styles.service.dto.StyleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Style} and its DTO {@link StyleDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ClothMapper.class})
public interface StyleMapper extends EntityMapper<StyleDTO, Style> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "cloths", target = "cloths")
    StyleDTO toDto(Style style);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "removeCloths", ignore = true)
    Style toEntity(StyleDTO styleDTO);

    default Style fromId(Long id) {
        if (id == null) {
            return null;
        }
        Style style = new Style();
        style.setId(id);
        return style;
    }
}
