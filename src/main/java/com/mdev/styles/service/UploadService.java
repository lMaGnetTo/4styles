package com.mdev.styles.service;

import com.mdev.styles.domain.Upload;
import com.mdev.styles.repository.UploadRepository;
import com.mdev.styles.service.dto.UploadDTO;
import com.mdev.styles.service.mapper.UploadMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Upload}.
 */
@Service
@Transactional
public class UploadService {

    private final Logger log = LoggerFactory.getLogger(UploadService.class);

    private final UploadRepository uploadRepository;

    private final UploadMapper uploadMapper;

    public UploadService(UploadRepository uploadRepository, UploadMapper uploadMapper) {
        this.uploadRepository = uploadRepository;
        this.uploadMapper = uploadMapper;
    }

    /**
     * Save a upload.
     *
     * @param uploadDTO the entity to save.
     * @return the persisted entity.
     */
    public UploadDTO save(UploadDTO uploadDTO) {
        log.debug("Request to save Upload : {}", uploadDTO);
        Upload upload = uploadMapper.toEntity(uploadDTO);
        upload = uploadRepository.save(upload);
        return uploadMapper.toDto(upload);
    }

    /**
     * Get all the uploads.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UploadDTO> findAll() {
        log.debug("Request to get all Uploads");
        return uploadRepository.findAll().stream()
            .map(uploadMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one upload by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UploadDTO> findOne(Long id) {
        log.debug("Request to get Upload : {}", id);
        return uploadRepository.findById(id)
            .map(uploadMapper::toDto);
    }

    /**
     * Delete the upload by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Upload : {}", id);
        uploadRepository.deleteById(id);
    }
}
