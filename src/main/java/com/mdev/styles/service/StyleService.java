package com.mdev.styles.service;

import com.mdev.styles.domain.Style;
import com.mdev.styles.repository.StyleRepository;
import com.mdev.styles.service.dto.ClothDTO;
import com.mdev.styles.service.dto.StyleDTO;
import com.mdev.styles.service.mapper.StyleMapper;
import com.mdev.styles.service.util.ClothingStylesBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Style}.
 */
@Service
@Transactional
public class StyleService {

    private final Logger log = LoggerFactory.getLogger(StyleService.class);

    private final StyleRepository styleRepository;

    private final StyleMapper styleMapper;

    private final ClothService clothService;

    public StyleService(StyleRepository styleRepository, StyleMapper styleMapper, ClothService clothService) {
        this.styleRepository = styleRepository;
        this.styleMapper = styleMapper;
        this.clothService = clothService;
    }

    /**
     * Save a style.
     *
     * @param styleDTO the entity to save.
     * @return the persisted entity.
     */
    public StyleDTO save(StyleDTO styleDTO) {
        log.debug("Request to save Style : {}", styleDTO);
        Style style = styleMapper.toEntity(styleDTO);
        style = styleRepository.save(style);
        return styleMapper.toDto(style);
    }

    /**
     * Get all the styles.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<StyleDTO> findAll() {
        log.debug("Request to get all Styles");
        return styleRepository.findAllWithEagerRelationships().stream()
            .map(styleMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the styles with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<StyleDTO> findAllWithEagerRelationships(Pageable pageable) {
        return styleRepository.findAllWithEagerRelationships(pageable).map(styleMapper::toDto);
    }


    /**
     * Get one style by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StyleDTO> findOne(Long id) {
        log.debug("Request to get Style : {}", id);
        return styleRepository.findOneWithEagerRelationships(id)
            .map(styleMapper::toDto);
    }

    /**
     * Delete the style by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Style : {}", id);
        styleRepository.deleteById(id);
    }

    public List<StyleDTO> generateStyleInWardrobe(Long id) {
        log.debug("Request to generate Styles from Wardrobe : {}", id);
        List<ClothDTO> allInWardrobe = clothService.findAllInWardrobe(id);
        return new ClothingStylesBuilder(allInWardrobe, 10).buildStyles();
    }

    public List<StyleDTO> findByUser(Long id) {
        log.debug("Request to get Styles of User : {}", id);
        return styleRepository.findByUserId(id).stream()
            .map(styleMapper::toDto)
            .collect(Collectors.toList());
    }
}
