package com.mdev.styles.service;

import com.mdev.styles.domain.Cloth;
import com.mdev.styles.repository.ClothRepository;
import com.mdev.styles.service.dto.ClothDTO;
import com.mdev.styles.service.mapper.ClothMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Cloth}.
 */
@Service
@Transactional
public class ClothService {

    private final Logger log = LoggerFactory.getLogger(ClothService.class);

    private final ClothRepository clothRepository;

    private final ClothMapper clothMapper;

    public ClothService(ClothRepository clothRepository, ClothMapper clothMapper) {
        this.clothRepository = clothRepository;
        this.clothMapper = clothMapper;
    }

    /**
     * Save a cloth.
     *
     * @param clothDTO the entity to save.
     * @return the persisted entity.
     */
    public ClothDTO save(ClothDTO clothDTO) {
        log.debug("Request to save Cloth : {}", clothDTO);
        Cloth cloth = clothMapper.toEntity(clothDTO);
        cloth = clothRepository.save(cloth);
        return clothMapper.toDto(cloth);
    }

    public List<ClothDTO> save(List<ClothDTO> clothDTOs) {
        log.debug("Request to save Cloth : {}", clothDTOs);
        List<ClothDTO> saved = new LinkedList<>();
        List<Cloth> cloths = clothMapper.toEntity(clothDTOs);
        cloths.forEach(cloth -> {
            saved.add(clothMapper.toDto(clothRepository.save(cloth)));
        });
        return saved;
    }

    /**
     * Get all the cloths.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ClothDTO> findAll() {
        log.debug("Request to get all Cloths");
        return clothRepository.findAll().stream()
            .map(clothMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one cloth by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ClothDTO> findOne(Long id) {
        log.debug("Request to get Cloth : {}", id);
        return clothRepository.findById(id)
            .map(clothMapper::toDto);
    }

    /**
     * Delete the cloth by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Cloth : {}", id);
        clothRepository.deleteById(id);
    }

    public List<ClothDTO> findAllInWardrobe(Long id) {
        return clothRepository.findAllByWardrobeId(id).stream()
            .map(clothMapper::toDto)
            .collect(Collectors.toList());
    }
}
