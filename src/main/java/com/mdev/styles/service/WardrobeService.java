package com.mdev.styles.service;

import com.mdev.styles.domain.Wardrobe;
import com.mdev.styles.repository.WardrobeRepository;
import com.mdev.styles.service.dto.WardrobeDTO;
import com.mdev.styles.service.mapper.WardrobeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Wardrobe}.
 */
@Service
@Transactional
public class WardrobeService {

    private final Logger log = LoggerFactory.getLogger(WardrobeService.class);

    private final WardrobeRepository wardrobeRepository;

    private final WardrobeMapper wardrobeMapper;

    public WardrobeService(WardrobeRepository wardrobeRepository, WardrobeMapper wardrobeMapper) {
        this.wardrobeRepository = wardrobeRepository;
        this.wardrobeMapper = wardrobeMapper;
    }

    /**
     * Save a wardrobe.
     *
     * @param wardrobeDTO the entity to save.
     * @return the persisted entity.
     */
    public WardrobeDTO save(WardrobeDTO wardrobeDTO) {
        log.debug("Request to save Wardrobe : {}", wardrobeDTO);
        Wardrobe wardrobe = wardrobeMapper.toEntity(wardrobeDTO);
        wardrobe = wardrobeRepository.save(wardrobe);
        return wardrobeMapper.toDto(wardrobe);
    }

    /**
     * Get all the wardrobes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<WardrobeDTO> findAll() {
        log.debug("Request to get all Wardrobes");
        return wardrobeRepository.findAll().stream()
            .map(wardrobeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one wardrobe by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<WardrobeDTO> findOne(Long id) {
        log.debug("Request to get Wardrobe : {}", id);
        return wardrobeRepository.findById(id)
            .map(wardrobeMapper::toDto);
    }

    /**
     * Delete the wardrobe by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Wardrobe : {}", id);
        wardrobeRepository.deleteById(id);
    }

    public List<WardrobeDTO> findByUser(Long id) {
        return wardrobeRepository.findByUserId(id).stream()
            .map(wardrobeMapper::toDto)
            .collect(Collectors.toList());
    }
}
