package com.mdev.styles.service;

import com.mdev.styles.domain.ClothType;
import com.mdev.styles.repository.ClothTypeRepository;
import com.mdev.styles.service.dto.ClothTypeDTO;
import com.mdev.styles.service.mapper.ClothTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ClothType}.
 */
@Service
@Transactional
public class ClothTypeService {

    private final Logger log = LoggerFactory.getLogger(ClothTypeService.class);

    private final ClothTypeRepository clothTypeRepository;

    private final ClothTypeMapper clothTypeMapper;

    public ClothTypeService(ClothTypeRepository clothTypeRepository, ClothTypeMapper clothTypeMapper) {
        this.clothTypeRepository = clothTypeRepository;
        this.clothTypeMapper = clothTypeMapper;
    }

    /**
     * Save a clothType.
     *
     * @param clothTypeDTO the entity to save.
     * @return the persisted entity.
     */
    public ClothTypeDTO save(ClothTypeDTO clothTypeDTO) {
        log.debug("Request to save ClothType : {}", clothTypeDTO);
        ClothType clothType = clothTypeMapper.toEntity(clothTypeDTO);
        clothType = clothTypeRepository.save(clothType);
        return clothTypeMapper.toDto(clothType);
    }

    /**
     * Get all the clothTypes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ClothTypeDTO> findAll() {
        log.debug("Request to get all ClothTypes");
        return clothTypeRepository.findAll().stream()
            .map(clothTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one clothType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ClothTypeDTO> findOne(Long id) {
        log.debug("Request to get ClothType : {}", id);
        return clothTypeRepository.findById(id)
            .map(clothTypeMapper::toDto);
    }

    /**
     * Delete the clothType by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ClothType : {}", id);
        clothTypeRepository.deleteById(id);
    }
}
