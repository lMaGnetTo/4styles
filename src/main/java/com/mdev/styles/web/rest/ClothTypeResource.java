package com.mdev.styles.web.rest;

import com.mdev.styles.service.ClothTypeService;
import com.mdev.styles.web.rest.errors.BadRequestAlertException;
import com.mdev.styles.service.dto.ClothTypeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mdev.styles.domain.ClothType}.
 */
@RestController
@RequestMapping("/api")
public class ClothTypeResource {

    private final Logger log = LoggerFactory.getLogger(ClothTypeResource.class);

    private static final String ENTITY_NAME = "clothType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClothTypeService clothTypeService;

    public ClothTypeResource(ClothTypeService clothTypeService) {
        this.clothTypeService = clothTypeService;
    }

    /**
     * {@code POST  /cloth-types} : Create a new clothType.
     *
     * @param clothTypeDTO the clothTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clothTypeDTO, or with status {@code 400 (Bad Request)} if the clothType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cloth-types")
    public ResponseEntity<ClothTypeDTO> createClothType(@RequestBody ClothTypeDTO clothTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ClothType : {}", clothTypeDTO);
        if (clothTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new clothType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClothTypeDTO result = clothTypeService.save(clothTypeDTO);
        return ResponseEntity.created(new URI("/api/cloth-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cloth-types} : Updates an existing clothType.
     *
     * @param clothTypeDTO the clothTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clothTypeDTO,
     * or with status {@code 400 (Bad Request)} if the clothTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clothTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cloth-types")
    public ResponseEntity<ClothTypeDTO> updateClothType(@RequestBody ClothTypeDTO clothTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ClothType : {}", clothTypeDTO);
        if (clothTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClothTypeDTO result = clothTypeService.save(clothTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clothTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cloth-types} : get all the clothTypes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clothTypes in body.
     */
    @GetMapping("/cloth-types")
    public List<ClothTypeDTO> getAllClothTypes() {
        log.debug("REST request to get all ClothTypes");
        return clothTypeService.findAll();
    }

    /**
     * {@code GET  /cloth-types/:id} : get the "id" clothType.
     *
     * @param id the id of the clothTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clothTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cloth-types/{id}")
    public ResponseEntity<ClothTypeDTO> getClothType(@PathVariable Long id) {
        log.debug("REST request to get ClothType : {}", id);
        Optional<ClothTypeDTO> clothTypeDTO = clothTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clothTypeDTO);
    }

    /**
     * {@code DELETE  /cloth-types/:id} : delete the "id" clothType.
     *
     * @param id the id of the clothTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cloth-types/{id}")
    public ResponseEntity<Void> deleteClothType(@PathVariable Long id) {
        log.debug("REST request to delete ClothType : {}", id);
        clothTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
