package com.mdev.styles.web.rest;

import com.mdev.styles.service.StyleService;
import com.mdev.styles.web.rest.errors.BadRequestAlertException;
import com.mdev.styles.service.dto.StyleDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mdev.styles.domain.Style}.
 */
@RestController
@RequestMapping("/api")
public class StyleResource {

    private final Logger log = LoggerFactory.getLogger(StyleResource.class);

    private static final String ENTITY_NAME = "style";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StyleService styleService;

    public StyleResource(StyleService styleService) {
        this.styleService = styleService;
    }

    /**
     * {@code POST  /styles} : Create a new style.
     *
     * @param styleDTO the styleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new styleDTO, or with status {@code 400 (Bad Request)} if the style has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/styles")
    public ResponseEntity<StyleDTO> createStyle(@RequestBody StyleDTO styleDTO) throws URISyntaxException {
        log.debug("REST request to save Style : {}", styleDTO);
        if (styleDTO.getId() != null) {
            throw new BadRequestAlertException("A new style cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StyleDTO result = styleService.save(styleDTO);
        return ResponseUtil.wrapOrNotFound(styleService.findOne(result.getId()));
    }

    /**
     * {@code PUT  /styles} : Updates an existing style.
     *
     * @param styleDTO the styleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated styleDTO,
     * or with status {@code 400 (Bad Request)} if the styleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the styleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/styles")
    public ResponseEntity<StyleDTO> updateStyle(@RequestBody StyleDTO styleDTO) throws URISyntaxException {
        log.debug("REST request to update Style : {}", styleDTO);
        if (styleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StyleDTO result = styleService.save(styleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, styleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /styles} : get all the styles.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of styles in body.
     */
    @GetMapping("/styles")
    public List<StyleDTO> getAllStyles(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Styles");
        return styleService.findAll();
    }

    /**
     * {@code GET  /styles/:id} : get the "id" style.
     *
     * @param id the id of the styleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the styleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/styles/{id}")
    public ResponseEntity<StyleDTO> getStyle(@PathVariable Long id) {
        log.debug("REST request to get Style : {}", id);
        Optional<StyleDTO> styleDTO = styleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(styleDTO);
    }

    /**
     * {@code DELETE  /styles/:id} : delete the "id" style.
     *
     * @param id the id of the styleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/styles/{id}")
    public ResponseEntity<Void> deleteStyle(@PathVariable Long id) {
        log.debug("REST request to delete Style : {}", id);
        styleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
