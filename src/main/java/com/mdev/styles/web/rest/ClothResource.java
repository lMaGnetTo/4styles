package com.mdev.styles.web.rest;

import com.mdev.styles.service.ClothService;
import com.mdev.styles.web.rest.errors.BadRequestAlertException;
import com.mdev.styles.service.dto.ClothDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mdev.styles.domain.Cloth}.
 */
@RestController
@RequestMapping("/api")
public class ClothResource {

    private final Logger log = LoggerFactory.getLogger(ClothResource.class);

    private static final String ENTITY_NAME = "cloth";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClothService clothService;

    public ClothResource(ClothService clothService) {
        this.clothService = clothService;
    }

    /**
     * {@code POST  /cloths} : Create a new cloth.
     *
     * @param clothDTO the clothDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clothDTO, or with status {@code 400 (Bad Request)} if the cloth has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cloths")
    public ResponseEntity<ClothDTO> createCloth(@RequestBody ClothDTO clothDTO) throws URISyntaxException {
        log.debug("REST request to save Cloth : {}", clothDTO);
        if (clothDTO.getId() != null) {
            throw new BadRequestAlertException("A new cloth cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClothDTO result = clothService.save(clothDTO);
        return ResponseEntity.created(new URI("/api/cloths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/cloths/batch")
    public ResponseEntity<List<ClothDTO>> createCloth(@RequestBody List<ClothDTO> clothDTOs) throws URISyntaxException {
        log.debug("REST request to save Cloth : {}", clothDTOs);
        clothDTOs.forEach(clothDTO -> {
            if (clothDTO.getId() != null) {
                throw new BadRequestAlertException("A new cloth cannot already have an ID", ENTITY_NAME, "idexists");
            }
        });
        List<ClothDTO> save = clothService.save(clothDTOs);
        return ResponseEntity.ok(save);
    }

    /**
     * {@code PUT  /cloths} : Updates an existing cloth.
     *
     * @param clothDTO the clothDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clothDTO,
     * or with status {@code 400 (Bad Request)} if the clothDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clothDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cloths")
    public ResponseEntity<ClothDTO> updateCloth(@RequestBody ClothDTO clothDTO) throws URISyntaxException {
        log.debug("REST request to update Cloth : {}", clothDTO);
        if (clothDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClothDTO result = clothService.save(clothDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clothDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cloths} : get all the cloths.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cloths in body.
     */
    @GetMapping("/cloths")
    public List<ClothDTO> getAllCloths() {
        log.debug("REST request to get all Cloths");
        return clothService.findAll();
    }

    /**
     * {@code GET  /cloths/:id} : get the "id" cloth.
     *
     * @param id the id of the clothDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clothDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cloths/{id}")
    public ResponseEntity<ClothDTO> getCloth(@PathVariable Long id) {
        log.debug("REST request to get Cloth : {}", id);
        Optional<ClothDTO> clothDTO = clothService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clothDTO);
    }

    /**
     * {@code DELETE  /cloths/:id} : delete the "id" cloth.
     *
     * @param id the id of the clothDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cloths/{id}")
    public ResponseEntity<Void> deleteCloth(@PathVariable Long id) {
        log.debug("REST request to delete Cloth : {}", id);
        clothService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
