/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mdev.styles.web.rest.vm;
