package com.mdev.styles.web.rest;

import com.mdev.styles.service.ClothService;
import com.mdev.styles.service.StyleService;
import com.mdev.styles.service.WardrobeService;
import com.mdev.styles.service.dto.ClothDTO;
import com.mdev.styles.service.dto.StyleDTO;
import com.mdev.styles.service.dto.WardrobeDTO;
import com.mdev.styles.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mdev.styles.domain.Wardrobe}.
 */
@RestController
@RequestMapping("/api")
public class WardrobeResource {

    private final Logger log = LoggerFactory.getLogger(WardrobeResource.class);

    private static final String ENTITY_NAME = "wardrobe";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WardrobeService wardrobeService;

    private final StyleService styleService;

    private final ClothService clothService;

    public WardrobeResource(WardrobeService wardrobeService, ClothService clothService, StyleService styleService) {
        this.wardrobeService = wardrobeService;
        this.clothService = clothService;
        this.styleService = styleService; }

    /**
     * {@code POST  /wardrobes} : Create a new wardrobe.
     *
     * @param wardrobeDTO the wardrobeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wardrobeDTO, or with status {@code 400 (Bad Request)} if the wardrobe has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wardrobes")
    public ResponseEntity<WardrobeDTO> createWardrobe(@RequestBody WardrobeDTO wardrobeDTO) throws URISyntaxException {
        log.debug("REST request to save Wardrobe : {}", wardrobeDTO);
        if (wardrobeDTO.getId() != null) {
            throw new BadRequestAlertException("A new wardrobe cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WardrobeDTO result = wardrobeService.save(wardrobeDTO);
        return ResponseEntity.created(new URI("/api/wardrobes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wardrobes} : Updates an existing wardrobe.
     *
     * @param wardrobeDTO the wardrobeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wardrobeDTO,
     * or with status {@code 400 (Bad Request)} if the wardrobeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wardrobeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wardrobes")
    public ResponseEntity<WardrobeDTO> updateWardrobe(@RequestBody WardrobeDTO wardrobeDTO) throws URISyntaxException {
        log.debug("REST request to update Wardrobe : {}", wardrobeDTO);
        if (wardrobeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WardrobeDTO result = wardrobeService.save(wardrobeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, wardrobeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /wardrobes} : get all the wardrobes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wardrobes in body.
     */
    @GetMapping("/wardrobes")
    public List<WardrobeDTO> getAllWardrobes() {
        log.debug("REST request to get all Wardrobes");
        return wardrobeService.findAll();
    }

    /**
     * {@code GET  /wardrobes/:id} : get the "id" wardrobe.
     *
     * @param id the id of the wardrobeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wardrobeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wardrobes/{id}")
    public ResponseEntity<WardrobeDTO> getWardrobe(@PathVariable Long id) {
        log.debug("REST request to get Wardrobe : {}", id);
        Optional<WardrobeDTO> wardrobeDTO = wardrobeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wardrobeDTO);
    }

    /**
     * {@code DELETE  /wardrobes/:id} : delete the "id" wardrobe.
     *
     * @param id the id of the wardrobeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wardrobes/{id}")
    public ResponseEntity<Void> deleteWardrobe(@PathVariable Long id) {
        log.debug("REST request to delete Wardrobe : {}", id);
        wardrobeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /wardrobes/:id/cloths} : get all the cloths in wardrobe.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cloths in body.
     */
    @GetMapping("/wardrobes/{id}/cloths")
    public List<ClothDTO> getAllCloths(@PathVariable Long id) {
        log.debug("REST request to get all Cloths in a Wardrobe : {}", id);
        return clothService.findAllInWardrobe(id);
    }

    /**
     * {@code GET  /wardrobes/:id/styles} : generate styles from cloth in a wardrobe.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cloths in body.
     */
    @GetMapping("/wardrobes/{id}/styles")
    public List<StyleDTO> generateStyles(@PathVariable Long id) {
        log.debug("REST request to generate Styles from a Wardrobe : {}", id);
        return styleService.generateStyleInWardrobe(id);
    }
}
