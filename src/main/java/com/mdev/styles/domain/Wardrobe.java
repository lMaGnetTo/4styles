package com.mdev.styles.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Wardrobe.
 */
@Entity
@Table(name = "wardrobe")
public class Wardrobe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "logo_url")
    private String logoUrl;

    @OneToMany(mappedBy = "wardrobe")
    private Set<Cloth> cloths = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("wardrobes")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Wardrobe name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public Wardrobe logoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
        return this;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Set<Cloth> getCloths() {
        return cloths;
    }

    public Wardrobe cloths(Set<Cloth> cloths) {
        this.cloths = cloths;
        return this;
    }

    public Wardrobe addCloth(Cloth cloth) {
        this.cloths.add(cloth);
        cloth.setWardrobe(this);
        return this;
    }

    public Wardrobe removeCloth(Cloth cloth) {
        this.cloths.remove(cloth);
        cloth.setWardrobe(null);
        return this;
    }

    public void setCloths(Set<Cloth> cloths) {
        this.cloths = cloths;
    }

    public User getUser() {
        return user;
    }

    public Wardrobe user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wardrobe)) {
            return false;
        }
        return id != null && id.equals(((Wardrobe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Wardrobe{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", logoUrl='" + getLogoUrl() + "'" +
            "}";
    }
}
