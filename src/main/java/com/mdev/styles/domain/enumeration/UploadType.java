package com.mdev.styles.domain.enumeration;

/**
 * The UploadType enumeration.
 */
public enum UploadType {
    CLOTH_MAIN, CLOTH_SECONDARY
}
