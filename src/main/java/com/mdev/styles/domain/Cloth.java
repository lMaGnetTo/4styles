package com.mdev.styles.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cloth.
 */
@Entity
@Table(name = "cloth")
public class Cloth implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "favourite")
    private Boolean favourite;

    @Column(name = "available")
    private Boolean available;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @OneToMany(mappedBy = "cloth")
    private Set<Upload> uploads = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("cloths")
    private Wardrobe wardrobe;

    @ManyToOne
    @JsonIgnoreProperties("cloths")
    private ClothType clothType;

    @ManyToMany(mappedBy = "cloths")
    @JsonIgnore
    private Set<Style> styles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Cloth name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isFavourite() {
        return favourite;
    }

    public Cloth favourite(Boolean favourite) {
        this.favourite = favourite;
        return this;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public Boolean isAvailable() {
        return available;
    }

    public Cloth available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public byte[] getImage() {
        return image;
    }

    public Cloth image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Cloth imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Set<Upload> getUploads() {
        return uploads;
    }

    public Cloth uploads(Set<Upload> uploads) {
        this.uploads = uploads;
        return this;
    }

    public Cloth addUploads(Upload upload) {
        this.uploads.add(upload);
        upload.setCloth(this);
        return this;
    }

    public Cloth removeUploads(Upload upload) {
        this.uploads.remove(upload);
        upload.setCloth(null);
        return this;
    }

    public void setUploads(Set<Upload> uploads) {
        this.uploads = uploads;
    }

    public Wardrobe getWardrobe() {
        return wardrobe;
    }

    public Cloth wardrobe(Wardrobe wardrobe) {
        this.wardrobe = wardrobe;
        return this;
    }

    public void setWardrobe(Wardrobe wardrobe) {
        this.wardrobe = wardrobe;
    }

    public ClothType getClothType() {
        return clothType;
    }

    public Cloth clothType(ClothType clothType) {
        this.clothType = clothType;
        return this;
    }

    public void setClothType(ClothType clothType) {
        this.clothType = clothType;
    }

    public Set<Style> getStyles() {
        return styles;
    }

    public Cloth styles(Set<Style> styles) {
        this.styles = styles;
        return this;
    }

    public Cloth addStyles(Style style) {
        this.styles.add(style);
        style.getCloths().add(this);
        return this;
    }

    public Cloth removeStyles(Style style) {
        this.styles.remove(style);
        style.getCloths().remove(this);
        return this;
    }

    public void setStyles(Set<Style> styles) {
        this.styles = styles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cloth)) {
            return false;
        }
        return id != null && id.equals(((Cloth) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cloth{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", favourite='" + isFavourite() + "'" +
            ", available='" + isAvailable() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            "}";
    }
}
