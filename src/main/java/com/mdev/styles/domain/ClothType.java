package com.mdev.styles.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ClothType.
 */
@Entity
@Table(name = "cloth_type")
public class ClothType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "deepfashion_alias")
    private String deepfashionAlias;

    @OneToMany(mappedBy = "clothType")
    private Set<Cloth> cloths = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClothType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeepfashionAlias() {
        return deepfashionAlias;
    }

    public ClothType deepfashionAlias(String deepfashionAlias) {
        this.deepfashionAlias = deepfashionAlias;
        return this;
    }

    public void setDeepfashionAlias(String deepfashionAlias) {
        this.deepfashionAlias = deepfashionAlias;
    }

    public Set<Cloth> getCloths() {
        return cloths;
    }

    public ClothType cloths(Set<Cloth> cloths) {
        this.cloths = cloths;
        return this;
    }

    public ClothType addCloths(Cloth cloth) {
        this.cloths.add(cloth);
        cloth.setClothType(this);
        return this;
    }

    public ClothType removeCloths(Cloth cloth) {
        this.cloths.remove(cloth);
        cloth.setClothType(null);
        return this;
    }

    public void setCloths(Set<Cloth> cloths) {
        this.cloths = cloths;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClothType)) {
            return false;
        }
        return id != null && id.equals(((ClothType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClothType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", deepfashionAlias='" + getDeepfashionAlias() + "'" +
            "}";
    }
}
