package com.mdev.styles.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Style.
 */
@Entity
@Table(name = "style")
public class Style implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "created")
    private Instant created;

    @ManyToOne
    @JsonIgnoreProperties("styles")
    private User user;

    @ManyToMany
    @JoinTable(name = "style_cloths",
               joinColumns = @JoinColumn(name = "style_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "cloths_id", referencedColumnName = "id"))
    private Set<Cloth> cloths = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Style name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreated() {
        return created;
    }

    public Style created(Instant created) {
        this.created = created;
        return this;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public User getUser() {
        return user;
    }

    public Style user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Cloth> getCloths() {
        return cloths;
    }

    public Style cloths(Set<Cloth> cloths) {
        this.cloths = cloths;
        return this;
    }

    public Style addCloths(Cloth cloth) {
        this.cloths.add(cloth);
        cloth.getStyles().add(this);
        return this;
    }

    public Style removeCloths(Cloth cloth) {
        this.cloths.remove(cloth);
        cloth.getStyles().remove(this);
        return this;
    }

    public void setCloths(Set<Cloth> cloths) {
        this.cloths = cloths;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Style)) {
            return false;
        }
        return id != null && id.equals(((Style) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Style{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
