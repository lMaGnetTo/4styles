package com.mdev.styles.repository;
import com.mdev.styles.domain.ClothType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ClothType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClothTypeRepository extends JpaRepository<ClothType, Long> {

}
