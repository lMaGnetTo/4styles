package com.mdev.styles.repository;
import com.mdev.styles.domain.Cloth;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Cloth entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClothRepository extends JpaRepository<Cloth, Long> {

    List<Cloth> findAllByWardrobeId(Long id);

}
