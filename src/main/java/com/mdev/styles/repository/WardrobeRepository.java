package com.mdev.styles.repository;
import com.mdev.styles.domain.Wardrobe;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Wardrobe entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WardrobeRepository extends JpaRepository<Wardrobe, Long> {

    @Query("select wardrobe from Wardrobe wardrobe where wardrobe.user.login = ?#{principal.username}")
    List<Wardrobe> findByUserIsCurrentUser();

    List<Wardrobe> findByUserId(Long id);

}
