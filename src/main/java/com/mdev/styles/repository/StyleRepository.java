package com.mdev.styles.repository;
import com.mdev.styles.domain.Style;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Style entity.
 */
@Repository
public interface StyleRepository extends JpaRepository<Style, Long> {

    @Query("select style from Style style where style.user.login = ?#{principal.username}")
    List<Style> findByUserIsCurrentUser();

    List<Style> findByUserId(Long id);

    @Query(value = "select distinct style from Style style left join fetch style.cloths",
        countQuery = "select count(distinct style) from Style style")
    Page<Style> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct style from Style style left join fetch style.cloths")
    List<Style> findAllWithEagerRelationships();

    @Query("select style from Style style left join fetch style.cloths where style.id =:id")
    Optional<Style> findOneWithEagerRelationships(@Param("id") Long id);

}
